package com.getjavajob.training.algo08.korneeva.lesson08;

import com.getjavajob.training.algo08.korneeva.lesson07.Node;

import java.util.Comparator;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 07.11.16.
 */
public class BinarySearchTreeTest {
    public static void main(String[] args) {
        testCompare();
        testBinarySearch();
    }

    public static void testCompare() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer a1, Integer a2) {
                return a1 - a2;
            }
        });
        assertEquals("BinarySearchTreeTest.testCompareMore", true, bst.compare(12, 3) > 0);
        assertEquals("BinarySearchTreeTest.testCompareLess", false, bst.compare(3, 12) > 0);
        assertEquals("BinarySearchTreeTest.testCompareEquals", true, bst.compare(12, 12) == 0);
    }

    public static void testBinarySearch() {
        BinarySearchTree<Integer> bst = new BinarySearchTree<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer a1, Integer a2) {
                return a1 - a2;
            }
        });

        Node<Integer> node = bst.addRoot(11);
        bst.addLeft(bst.root(), 8);
        bst.addRight(bst.root(), 15);
        assertEquals("BinarySearchTreeTest.testBinarySearch", 15, bst.treeSearch(node, 15).getElement());
    }
}
