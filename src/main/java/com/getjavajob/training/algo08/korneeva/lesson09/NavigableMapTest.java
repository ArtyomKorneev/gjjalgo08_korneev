package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.Collections;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.11.16.
 */
public class NavigableMapTest {
    private static NavigableMap<Integer, String> map = new TreeMap<>();

    public static void main(String[] args) {
        mapInit();
        testLowerEntry();
        testFloorEntry();
        testCeilingEntry();
        testHigherEntry();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
    }

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
    }

    private static void testLowerEntry() {
        Map.Entry<Integer, String> m = map.lowerEntry(3);
        assertEquals("NavigableMapTest.testLowerEntry", "2=two", m.toString());
    }

    private static void testFloorEntry() {
        Map.Entry<Integer, String> m = map.floorEntry(4);
        assertEquals("NavigableMapTest.testFloorEntry", "4=four", m.toString());
    }

    private static void testCeilingEntry() {
        assertEquals("NavigableMapTest.testCeilingEntry", "four", map.ceilingEntry(4).getValue());
    }

    private static void testHigherEntry() {
        assertEquals("NavigableMapTest.testHigherEntry", "five", map.higherEntry(4).getValue());
    }

    private static void testFirstEntry() {
        assertEquals("NavigableMapTest.testFirstEntry", "one", map.firstEntry().getValue());
    }

    private static void testLastEntry() {
        assertEquals("NavigableMapTest.testLastEntry", "five", map.lastEntry().getValue());
    }

    private static void testPollFirstEntry() {
        assertEquals("NavigableMapTest.testPollFirstEntry", "one", map.pollFirstEntry().getValue());
        map.put(1, "one");
    }

    private static void testPollLastEntry() {
        assertEquals("NavigableMapTest.testPollLastEntry", "five", map.pollLastEntry().getValue());
        map.put(5, "five");
    }

    private static void testDescendingMap() {
        NavigableMap<Integer, String> pam = new TreeMap<>(Collections.reverseOrder());
        pam.putAll(map);
        assertEquals("NavigableMapTest.testDescendingMap", pam, map.descendingMap());
    }

    private static void testNavigableKeySet() {
        NavigableSet<Integer> m = map.navigableKeySet();
        assertEquals("NavigableMapTest.testNavigableKeySet", m.toString(), "[1, 2, 3, 4, 5]");
    }

    private static void testDescendingKeySet() {
        NavigableSet<Integer> m = map.descendingKeySet();
        assertEquals("NavigableMapTest.testDescendingKeySet", m.toString(), "[5, 4, 3, 2, 1]");
    }
}
