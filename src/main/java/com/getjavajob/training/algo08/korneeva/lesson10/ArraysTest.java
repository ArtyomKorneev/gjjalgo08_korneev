package com.getjavajob.training.algo08.korneeva.lesson10;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by ZinZaga on 17.11.16.
 */
public class ArraysTest {
    @Test
    public void sort() throws Exception {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
        String[] s = {"a", "B", "d", "c", "4", " ", "Q", "?"};
        Arrays.sort(s);
        System.out.println(Arrays.toString(s));
    }

    @Test
    public void binarySearch() throws Exception {
        int[] array = {3, 5, 6, 8, 10, 1, 2, 4, 7, 9};
        Arrays.sort(array);
        assertEquals(4, Arrays.binarySearch(array, 5));
    }

    @Test
    public void equals() throws Exception {
        int[] array = {3, 5, 6, 8, 10, 1, 2, 4, 7, 9};
        assertEquals(true, Arrays.equals(array, new int[]{3, 5, 6, 8, 10, 1, 2, 4, 7, 9}));
    }

    @Test
    public void fill() throws Exception {
        int[] array = new int[10];
        Arrays.fill(array, 1);
        System.out.println(Arrays.toString(array));
    }

    @Test
    public void copyOf() throws Exception {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        int[] second = Arrays.copyOf(array, array.length);
        System.out.println(Arrays.toString(second));
    }

    @Test
    public void copyOfRange() throws Exception {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        int[] second = Arrays.copyOfRange(array, 3, array.length);
        System.out.println(Arrays.toString(second));
    }

    @Test
    public void asList() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        System.out.println(list);
    }

    @Test
    public void deepEquals() throws Exception {
        int[][] array = new int[4][4];
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                array[i][j] = k;
                k++;
            }
        }
        int[][] second = Arrays.copyOf(array, array.length);
        assertEquals(true, Arrays.deepEquals(array, second));
    }

    @Test
    public void deepToString() throws Exception {
        int[][] array = new int[4][4];
        int k = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                array[i][j] = k;
                k++;
            }
        }
        System.out.println(Arrays.deepToString(array));
    }
}