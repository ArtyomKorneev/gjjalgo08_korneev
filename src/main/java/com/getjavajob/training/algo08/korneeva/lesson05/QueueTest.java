package com.getjavajob.training.algo08.korneeva.lesson05;

import java.util.ArrayDeque;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

/**
 * Created by ZinZaga on 10.10.16.
 */
public class QueueTest {
    public static void main(String[] args) {
        addTest();
        offerTest();
        removeTest();
        pollTest();
        elementTest();
        peekTest();
    }

    private static void addTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        assertEquals("addTest", true, queue.add(157));
    }

    private static void offerTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        assertEquals("offerTest", true, queue.offer(157));
    }

    private static void removeTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        String msg = "NoSuchElementException";
        try {
            queue.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("removeTestFail", msg, e.getClass().getSimpleName());
        }

        queue.add(144);
        try {
            queue.remove();
            fail(msg);
        } catch (Exception e) {
            assertEquals("removeTestFail", msg, e.getClass().getSimpleName());
        } catch (AssertionError e) {
            assertEquals("removeTestOk", "AssertionError", e.getClass().getSimpleName());
        }
    }

    private static void pollTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        String msg = "NullPointerException";
        try {
            int i = queue.poll();
            fail(msg);
        } catch (Exception e) {
            assertEquals("pollTest", msg, e.getClass().getSimpleName());
        }
    }

    private static void elementTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        String msg = "NoSuchElementException";
        try {
            int i = queue.element();
            fail(msg);
        } catch (Exception e) {
            assertEquals("elementTest", msg, e.getClass().getSimpleName());
        }
    }

    private static void peekTest() {
        ArrayDeque<Integer> queue = new ArrayDeque<>();
        String msg = "NullPointerException";
        try {
            int k = queue.peek();
            fail(msg);
        } catch (Exception e) {
            assertEquals("peekTest", msg, e.getClass().getSimpleName());
        }
    }
}
