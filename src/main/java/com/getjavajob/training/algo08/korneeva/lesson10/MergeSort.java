package com.getjavajob.training.algo08.korneeva.lesson10;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class MergeSort {
    public static void mergeSort(int[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private static void sort(int[] arr, int low, int high) {
        if (low < high) {
            int middle = low + (high - low) / 2;
            sort(arr, low, middle);
            sort(arr, middle + 1, high);
            merge(arr, low, middle, high);
        }
    }

    private static void merge(int[] arr, int low, int middle, int high) {
        int[] helper = new int[arr.length];
        for (int i = low; i <= high; i++) {
            helper[i] = arr[i];
        }
        int i = low;
        int j = middle + 1;
        int k = low;
        while (i <= middle && j <= high) {
            if (helper[i] <= helper[j]) {
                arr[k] = helper[i];
                i++;
            } else {
                arr[k] = helper[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            arr[k] = helper[i];
            k++;
            i++;
        }
    }
}
