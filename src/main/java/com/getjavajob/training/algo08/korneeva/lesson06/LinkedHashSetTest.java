package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.LinkedHashSet;
import java.util.Set;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 25.10.16.
 */
public class LinkedHashSetTest {
    public static void main(String[] args) {
        Set<Integer> linkedSet = new LinkedHashSet<>();
        assertEquals("LinkedHashSetTest.addTrue", true, linkedSet.add(12));
        assertEquals("LinkedHashSetTest.addFalse", false, linkedSet.add(12));
        Set<Integer> nonSpecificTest = new LinkedHashSet<>();
        nonSpecificTest.add(1);
        nonSpecificTest.add(2);
        nonSpecificTest.add(3);
        nonSpecificTest.add(4);
        nonSpecificTest.add(5);
        nonSpecificTest.add(6);
        nonSpecificTest.add(7);
        System.out.println(nonSpecificTest);
    }
}
