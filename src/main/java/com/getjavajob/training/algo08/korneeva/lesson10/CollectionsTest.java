package com.getjavajob.training.algo08.korneeva.lesson10;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by ZinZaga on 17.11.16.
 */
public class CollectionsTest {
    @Test
    public void sort() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        Collections.sort(list);
        assertEquals(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9), list);
    }

    @Test
    public void sort1() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        Collections.sort(list, new Comparator<Integer>() {
            @Override
            public int compare(Integer a, Integer b) {
                return a < b ? 1 : a == b ? 0 : -1;
            }
        });
        assertEquals(Arrays.asList(9, 8, 7, 6, 5, 4, 3, 2, 1, 0), list);
    }

    @Test
    public void binarySearch() throws Exception {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        assertEquals(3, Collections.binarySearch(list, 4));
    }

    @Test
    public void reverse() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.reverse(list);
        assertEquals(Arrays.asList(9, 8, 7, 6, 5, 4, 3, 2, 1, 0), list);
    }

    @Test
    public void shuffle() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.shuffle(list);
        System.out.println(list);
    }

    @Test
    public void swap() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.swap(list, 1, 3);
        assertEquals(Arrays.asList(0, 3, 2, 1, 4, 5, 6, 7, 8, 9), list);
    }

    @Test
    public void fill() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.fill(list, 111);
        assertEquals(Arrays.asList(111, 111, 111, 111, 111, 111, 111, 111, 111, 111), list);
    }

    @Test
    public void copy() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<Integer> dest = Arrays.asList(0, 3, 2, 1, 4, 5, 6, 7, 8, 9);
        Collections.copy(dest, list);
        assertEquals(list, dest);
    }

    @Test
    public void min() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        int i = Collections.min(list);
        assertEquals(0, i);
    }

    @Test
    public void max() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        int i = Collections.max(list);
        assertEquals(9, i);
    }

    @Test
    public void rotate() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.rotate(list, 3);
        assertEquals(Arrays.asList(7, 8, 9, 0, 1, 2, 3, 4, 5, 6), list);
    }

    @Test
    public void replaceAll() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 1, 4, 5, 1, 7, 8, 1);
        Collections.replaceAll(list, 1, 999);
        assertEquals(Arrays.asList(0, 999, 2, 999, 4, 5, 999, 7, 8, 999), list);
    }

    @Test
    public void indexOfSubList() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<Integer> target = Arrays.asList(3, 4, 5, 6);
        int i = Collections.indexOfSubList(list, target);
        assertEquals(3, i);
    }

    @Test
    public void lastIndexOfSubList() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 0, 1, 2, 3);
        List<Integer> target = Arrays.asList(0, 1, 2, 3);
        int i = Collections.lastIndexOfSubList(list, target);
        assertEquals(4, i);
    }

    @Test
    public void unmodifiableCollection() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        Collections.unmodifiableCollection(list);
        try {
            list.remove(8);
        } catch (UnsupportedOperationException e) {
            System.out.println("Read-only");
        }
        System.out.println(list);
    }

    @Test
    public void emptySet() throws Exception {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set = Collections.emptySet();
        assertEquals(new HashSet<Integer>(), set);
    }

    @Test
    public void singleton() throws Exception {
        Set<String> set = Collections.singleton("Java");
        assertEquals(new HashSet<>(Arrays.asList("Java")), set);
    }

    @Test
    public void nCopies() throws Exception {
        Integer init = new Integer(-1);
        List<Integer> values = new ArrayList<>(Collections.nCopies(10, init));
        assertEquals(Arrays.asList(-1, -1, -1, -1, -1, -1, -1, -1, -1, -1), values);
    }

    @Test
    public void reverseOrder() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        Comparator<Integer> cmp = Collections.reverseOrder();
        Collections.sort(list, cmp);
        assertEquals(Arrays.asList(9, 8, 7, 6, 5, 4, 3, 2, 1, 0), list);
    }

    @Test
    public void frequency() throws Exception {
        List<Integer> list = Arrays.asList(0, 1, 2, 1, 4, 5, 1, 7, 8, 1);
        assertEquals(4, Collections.frequency(list, 1));
    }

    @Test
    public void disjoint() throws Exception {
        List<Integer> list = Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9);
        List<Integer> second = Arrays.asList(0, 1, 2, 1, 4, 5, 1, 7, 8, 1);
        assertEquals(false, Collections.disjoint(list, second));
    }

    @Test
    public void addAll() throws Exception {
        List<Integer> list = new ArrayList<>(Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9));
        Collections.addAll(list, 111, 222, 333, 444, 555);
        assertEquals(Arrays.asList(3, 5, 6, 8, 0, 1, 2, 4, 7, 9, 111, 222, 333, 444, 555), list);
    }

    @Test
    public void newSetFromMap() throws Exception {
        Map<String, Boolean> map = new HashMap<>();
        Set<String> set = Collections.newSetFromMap(map);
        map.put("zero", true);
        map.put("one", false);
        map.put("two", true);
        map.put("three", false);
        System.out.println(set);
    }
}