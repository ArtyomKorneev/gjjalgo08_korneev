package com.getjavajob.training.algo08.korneeva.lesson06;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.HashMap;

/**
 * Created by ZinZaga on 24.10.16.
 */
public class AssociativeArrayPerfomanceTest {
    public static void main(String[] args) {
        AssociativeArray<Integer, Integer> aa = new AssociativeArray();
        HashMap<Integer, Integer> map = new HashMap<>();
        testPutMethod(aa, map);
        testGetMethod(aa, map);
        testRemoveMethod(aa, map);
    }

    public static void testPutMethod(AssociativeArray associativeArray, HashMap hashMap) {
        StopWatch timer = new StopWatch();
        System.out.println("Addition to the map test");
        System.out.println("==============================");
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.add(i, i);
        }
        System.out.println("AssociativeArray.add(K,V): " + timer.getElapsedTime());
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            hashMap.put(i, i);
        }
        System.out.println("HashMap.put(K,V): " + timer.getElapsedTime());
    }

    public static void testGetMethod(AssociativeArray associativeArray, HashMap hashMap) {
        StopWatch timer = new StopWatch();
        System.out.println("\nGet from the map test");
        System.out.println("==============================");
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.add(i, i);
        }
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.get(i);
        }
        System.out.println("AssociativeArray.get(K): " + timer.getElapsedTime());
        for (int i = 0; i < 1_000_000; i++) {
            hashMap.put(i, i);
        }
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            hashMap.get(i);
        }
        System.out.println("HashMap.get(K): " + timer.getElapsedTime());
    }

    public static void testRemoveMethod(AssociativeArray associativeArray, HashMap hashMap) {
        StopWatch timer = new StopWatch();
        System.out.println("\nRemove from the map test");
        System.out.println("==============================");
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.add(i, i);
        }
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.remove(i);
        }
        System.out.println("AssociativeArray.remove(K): " + timer.getElapsedTime());
        for (int i = 0; i < 1_000_000; i++) {
            associativeArray.add(i, i);
        }
        timer.start();
        for (int i = 0; i < 1_000_000; i++) {
            hashMap.remove(i);
        }
        System.out.println("HashMap.remove(K): " + timer.getElapsedTime());
    }
}
