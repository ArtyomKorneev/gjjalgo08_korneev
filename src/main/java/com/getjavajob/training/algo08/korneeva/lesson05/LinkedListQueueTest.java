package com.getjavajob.training.algo08.korneeva.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.10.16.
 */
public class LinkedListQueueTest {
    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testAdd() {
        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        queue.add(0);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        assertEquals("LinkedListQueueTest.testRemove", "0 1 2 3 ", queue.asList());
    }

    private static void testRemove() {
        LinkedListQueue<Integer> queue = new LinkedListQueue<>();
        queue.add(0);
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.remove();
        queue.remove();
        assertEquals("LinkedListQueueTest.testRemove", "2 3 ", queue.asList());
    }
}
