package com.getjavajob.training.algo08.korneeva.lesson03;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.ArrayList;

/**
 * Created by ZinZaga on 29.09.16.
 */
public class DynamicArrayPerformanceTest {
    public static void main(String[] args) {
//        addRmvToBegin();
//        addRmvToMiddle();
        addRmvToEnd();
    }

    private static void addRmvToBegin() {
        StopWatch timer = new StopWatch();
        timer.start();
        DynamicArray my = new DynamicArray();
        for (int i = 0; i < 100_000; i++) {
            my.add(0, i);
        }
        System.out.println(timer.getElapsedTime() + " - DA, add to begin");

        timer.start();
        ArrayList<Integer> original = new ArrayList<>();
        for (int i = 0; i < 100_000; i++) {
            original.add(0, i);
        }
        System.out.println(timer.getElapsedTime() + " - AL, add to begin");

        timer.start();
        for (int i = 0; i < 100_000; i++) {
            my.remove(0);
        }
        System.out.println(timer.getElapsedTime() + " - DA, remove from begin");

        timer.start();
        for (int i = 0; i < 100_000; i++) {
            original.remove(0);
        }
        System.out.println(timer.getElapsedTime() + " - AL, remove from begin");
    }

    private static void addRmvToMiddle() {
        StopWatch timer = new StopWatch();
        timer.start();
        DynamicArray my = new DynamicArray();
        for (int i = 0; i < 100_000; i++) {
            my.add(my.size() / 2, i);
        }
        System.out.println(timer.getElapsedTime() + " - DA, add to middle");

        timer.start();
        ArrayList<Integer> original = new ArrayList<>();
        for (int i = 0; i < 100_000; i++) {
            original.add(original.size() / 2, i);
        }
        System.out.println(timer.getElapsedTime() + " - AL, add to middle");

        timer.start();
        for (int i = 0; i < 100_000; i++) {
            my.remove(my.size() / 2);
        }
        System.out.println(timer.getElapsedTime() + " - DA, remove from middle");

        timer.start();
        for (int i = 0; i < 100_000; i++) {
            original.remove(original.size() / 2);
        }
        System.out.println(timer.getElapsedTime() + " - AL, remove from middle");
    }

    private static void addRmvToEnd() {
        StopWatch timer = new StopWatch();
        timer.start();
        DynamicArray my = new DynamicArray();
        for (int i = 0; i < 5_000_000; i++) {
            my.add(i);
        }
        System.out.println(timer.getElapsedTime() + " - DA, add to end");

        timer.start();
        ArrayList<Integer> original = new ArrayList<>();
        for (int i = 0; i < 5_000_000; i++) {
            original.add(i);
        }
        System.out.println(timer.getElapsedTime() + " - AL, add to end");

        timer.start();
        for (int i = 0; i < 5_000_000; i++) {
            my.remove(my.size() - 1);
        }
        System.out.println(timer.getElapsedTime() + " - DA, remove from end");

        timer.start();
        for (int i = 0; i < 5_000_000; i++) {
            original.remove(original.size() - 1);
        }
        System.out.println(timer.getElapsedTime() + " - AL, remove from end");
    }
}
