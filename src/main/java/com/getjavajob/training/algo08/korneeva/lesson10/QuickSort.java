package com.getjavajob.training.algo08.korneeva.lesson10;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class QuickSort {
    public static void quickSort(int[] arr) {
        sort(arr, 0, arr.length - 1);
    }

    private static void sort(int[] arr, int left, int right) {
        int i = left;
        int j = right;
        int x = arr[left + (right - left) / 2];
        while (i <= j) {
            while (arr[i] < x) {
                i++;
            }
            while (arr[j] > x) {
                j--;
            }
            if (i <= j) {
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        if (left < j) {
            sort(arr, left, j);
        }
        if (i < right) {
            sort(arr, i, right);
        }
    }
}
