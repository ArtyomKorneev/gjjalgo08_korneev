package com.getjavajob.training.algo08.korneeva.lesson01;

import java.util.Scanner;

/**
 * Created by ZinZaga on 03.09.16.
 */
public class Task07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter two numbers:");
        int a = sc.nextInt();
        int b = sc.nextInt();
        System.out.println(swapForBitwiseTwo(a, b));
    }

    public static String swapForMathOne(int a, int b) {
        a = a + b;
        b = a - b;
        a = a - b;
        return a + " " + b;
    }

    public static String swapForMathTwo(int a, int b) {
        a = a * b;
        b = a / b;
        a = a / b;
        return a + " " + b;
    }

    public static String swapForBitwiseOne(int a, int b) {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        return a + " " + b;
    }

    public static String swapForBitwiseTwo(int a, int b) {
        a = ~a & b | ~b & a;
        b = ~a & b | ~b & a;
        a = ~a & b | ~b & a;
        return a + " " + b;
    }
}
