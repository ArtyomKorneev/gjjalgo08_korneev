package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.HashMap;

/**
 * Created by ZinZaga on 26.10.16.
 */
public class MatrixImplementation<Integer> implements Matrix<Integer> {
    private HashMap<KeyMatrix, Integer> matrix;

    public MatrixImplementation(HashMap<KeyMatrix, Integer> matrix) {
        this.matrix = matrix;
    }

    @Override
    public Integer get(int i, int j) {
        return matrix.get(new KeyMatrix(i, j));
    }

    @Override
    public void set(int i, int j, Integer value) {
        matrix.put(new KeyMatrix(i, j), value);
    }
}
