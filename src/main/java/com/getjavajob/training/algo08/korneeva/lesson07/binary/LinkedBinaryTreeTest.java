package com.getjavajob.training.algo08.korneeva.lesson07.binary;

import com.getjavajob.training.algo08.korneeva.lesson07.Node;

import java.util.ArrayList;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 31.10.16.
 */
public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testAdd();
        testAddLeft();
        testAddRight();
        testSetNode();
        testRemoveNode();
        testLeft();
        testRight();
        testParent();
        testPreorder();
        testInorder();
        testPostorder();
        testBreadthFirst();
    }

    private static void testBreadthFirst() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("G");
            add("A");
            add("D");
            add("I");
            add("C");
            add("E");
            add("H");
        }};
        for (Node<String> n : tree.breadthFirst()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testBreadthFirst", res, cmp);
    }

    private static void testPostorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("A");
            add("C");
            add("E");
            add("D");
            add("B");
            add("H");
            add("I");
            add("G");
            add("F");
        }};
        for (Node<String> n : tree.postOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testPostorder", res, cmp);
    }

    private static void testInorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("A");
            add("B");
            add("C");
            add("D");
            add("E");
            add("F");
            add("G");
            add("H");
            add("I");
        }};
        for (Node<String> n : tree.inOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testInorder", res, cmp);
    }

    private static void testPreorder() {
        ArrayBinaryTree<String> tree = new ArrayBinaryTree<>();
        tree.addRoot("F");
        tree.addRight(new ArrayBinaryTree.NodeImpl<String>(), "G");
        tree.addLeft(new ArrayBinaryTree.NodeImpl<String>(), "B");
        tree.addLeft(tree.left(tree.root()), "A");
        tree.addRight(tree.left(tree.root()), "D");

        tree.addLeft(tree.right(tree.left(tree.root())), "C");
        tree.addRight(tree.right(tree.left(tree.root())), "E");
        tree.addRight(tree.right(tree.root()), "I");
        tree.addLeft(tree.right(tree.right(tree.root())), "H");

        ArrayList<String> res = new ArrayList<>();
        ArrayList<String> cmp = new ArrayList<String>() {{
            add("F");
            add("B");
            add("A");
            add("D");
            add("C");
            add("E");
            add("G");
            add("I");
            add("H");
        }};
        for (Node<String> n : tree.preOrder()) {
            res.add(n.getElement());
        }
        assertEquals("LinkedBinaryTreeTest.testPreorder", res, cmp);
    }

    public static void testAddRoot() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRoot(17);
        assertEquals("LinkedBinaryTreeTest.testAddRoot", 17, node.getElement());
    }

    public static void testAdd() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRoot(17);
        assertEquals("LinkedBinaryTreeTest.testAdd", 19, tree.add(node, 19).getElement());
    }

    public static void testAddLeft() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRoot(17);
        assertEquals("LinkedBinaryTreeTest.testAddLeft", 19, tree.addLeft(node, 19).getElement());
    }

    public static void testAddRight() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRoot(17);
        assertEquals("LinkedBinaryTreeTest.testAddRight", 19, tree.addRight(node, 19).getElement());
    }

    public static void testSetNode() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRoot(17);
        assertEquals("LinkedBinaryTreeTest.testSet", 17, tree.set(node, 199));
    }

    public static void testRemoveNode() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(120);
        LinkedBinaryTree.NodeImpl<Integer> node = (LinkedBinaryTree.NodeImpl<Integer>) tree.addRight(tree.root, 17);
        assertEquals("LinkedBinaryTreeTest.testRemove", 17, tree.remove(node));
    }

    public static void testLeft() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(120);
        tree.addLeft(tree.root, 45);
        assertEquals("LinkedBinaryTreeTest.testLeft", 45, tree.left(tree.root()).getElement());
    }

    public static void testRight() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(120);
        tree.addRight(tree.root, 45);
        assertEquals("LinkedBinaryTreeTest.testRight", 45, tree.right(tree.root()).getElement());
    }

    public static void testParent() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        tree.addRoot(120);
        tree.addRight(tree.root, 45);
        assertEquals("LinkedBinaryTreeTest.testRight", 120, tree.parent(tree.root.right).getElement());
    }
}
