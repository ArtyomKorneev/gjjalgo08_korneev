package com.getjavajob.training.algo08.korneeva.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.korneeva.lesson10.QuickSort.quickSort;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class QuickSortTest {
    public static void main(String[] args) {
        testQuickSort();
    }

    private static void testQuickSort() {
        int[] array = {3, 5, 6, 8, 4, 1, 2, 0, 7, 9};
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }
}
