package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.HashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 24.10.16.
 */
public class MapTest {
    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap<>();

        assertEquals("MapTest.SizeTest", 0, map.size());
        assertEquals("MapTest.IsEmptyTest", true, map.isEmpty());
        map.put(1, 4);
        assertEquals("MapTest.ContainsKey", true, map.containsKey(1));
        assertEquals("MapTest.ContainsValue", true, map.containsValue(4));
        assertEquals("MapTest.getValue", 4, map.get(1));
        assertEquals("MapTest.putValue", 4, map.put(1, 7));
        map.put(0, 27);
        assertEquals("MapTest.Remove", 27, map.remove(0));
        HashMap<Integer, Integer> newMap = new HashMap<>();
        newMap.put(7, 34);
        newMap.put(2, 56);
        map.putAll(newMap);
        assertEquals("MapTest.putAll", 3, map.size());
        newMap.clear();
        assertEquals("MapTest.clear", 0, newMap.size());
        Integer[] test2 = {7, 56, 34};
        Integer[] my = new Integer[3];
        map.values().toArray(my);
        assertEquals("Maptest.Values", my, test2);
    }
}
