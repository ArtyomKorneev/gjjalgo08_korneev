package com.getjavajob.training.algo08.korneeva.lesson07.binary;

import com.getjavajob.training.algo08.korneeva.lesson07.AbstractTree;
import com.getjavajob.training.algo08.korneeva.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        if (isRoot(n)) {
            return null;
        }
        if (childrenNumber(parent(n)) == 2) {
            for (Node<E> sib : children(parent(n))) {
                if (!sib.equals(n)) {
                    return sib;
                }
            }
        }
        return null;
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        Collection<Node<E>> collection = new ArrayList<>();
        if (left(n) == null && right(n) == null) {
            return collection;
        }
        if (left(n) != null) {
            collection.add(left(n));
        }
        if (right(n) != null) {
            collection.add(right(n));
        }
        return collection;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        if (left(n) != null && right(n) != null) {
            return 2;
        } else if (left(n) != null || right(n) != null) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        if (size() == 0) {
            return new ArrayList<>();
        } else {
            return inOrder(root(), new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> inOrder(Node<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            inOrder(left(node), collection);
            collection.add(node);
            inOrder(right(node), collection);
        }
        return collection;
    }
}
