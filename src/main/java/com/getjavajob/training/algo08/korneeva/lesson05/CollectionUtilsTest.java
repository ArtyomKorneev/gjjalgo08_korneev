package com.getjavajob.training.algo08.korneeva.lesson05;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by ZinZaga on 17.10.16.
 */
public class CollectionUtilsTest extends Assert {
    private Collection<Employee> list;

    @Before
    public void setUp() {
        int i = 1;
        Employee[] a = {new Employee("Valera", "Semin", i++),
                new Employee("Stas", "Kutorkin", i++),
                new Employee("Arkasha", "Zorkin", i++),
                new Employee("Ivan", "Ivanov", i++),
                new Employee("Dima", "Shagalkin", i++)};
        list = new ArrayList<>(Arrays.asList(a));
    }

    @Test
    public void forAllDo() {
        CollectionUtils.forAllDo(list, new CollectionUtils.Closure<Employee>() {
            public void execute(Employee object) {
                int oldSalary = object.getSalary();
                object.setSalary(oldSalary + 100);
            }
        });

        for (Employee e : list) {
            assertTrue(e.getSalary() > 100);
        }
    }

    @Test
    public void transform() {
        final Employee president = new Employee("Vova", "Putin", 0);
        CollectionUtils.transform(list, new CollectionUtils.Transformer<Employee, Employee>() {
            public Employee transform(Employee object) {
                if (object.getFirstName().equals("Dima")) {
                    return president;
                }
                return object;
            }
        });

        assertTrue(list.contains(president));
    }

    @Test
    public void collection() {
        Collection<String> strings = CollectionUtils.collection(list,
                new CollectionUtils.Transformer<Employee, String>() {
                    @Override
                    public String transform(Employee object) {
                        return object.getLastName();
                    }
                });

        Collection<String> expectedStrings = new ArrayList<>(list.size());
        for (Employee e : list) {
            expectedStrings.add(e.getLastName());
        }

        assertEquals(strings, expectedStrings);
    }

    @Test
    public void filter() {
        assertTrue(CollectionUtils.filter(list, new CollectionUtils.Predicate<Employee>() {
            public boolean evaluate(Employee object) {
                return object.getLastName().equals("Ivanov");
            }
        }));

        assertEquals(list.size(), 1);
    }

    @Test(expected = Exception.class)
    public void unmodifiableCollection() {
        list = CollectionUtils.unmodifiableCollection(list);
        list.add(new Employee("Stas", "Stasov", 5));
    }

    class Employee {
        private String firstName;
        private String lastName;
        private int id;
        private int salary;

        public Employee(String firstName, String lastName, int id) {
            this(firstName, lastName, id, 100);
        }

        public Employee(String firstName, String lastName, int id, int salary) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.id = id;
            this.salary = salary;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSalary() {
            return salary;
        }

        public void setSalary(int salary) {
            this.salary = salary;
        }
    }
}
