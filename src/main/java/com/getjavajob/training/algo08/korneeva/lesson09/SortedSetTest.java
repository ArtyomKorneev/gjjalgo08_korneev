package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.11.16.
 */
public class SortedSetTest {
    static SortedSet<Integer> set = new TreeSet<>(Arrays.asList(1, 2, 3, 4));

    public static void main(String[] args) {
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    private static void testLast() {
        assertEquals("SortedSetTest.testLast", 4, set.last());
    }

    private static void testFirst() {
        assertEquals("SortedSetTest.testFirst", 1, set.first());
    }

    private static void testTailSet() {
        SortedSet<Integer> tailSet = new TreeSet<>(Arrays.asList(2, 3, 4));
        assertEquals("SortedSetTest.testTailSet", tailSet, set.tailSet(2));
    }

    private static void testHeadSet() {
        SortedSet<Integer> headSet = new TreeSet<>(Arrays.asList(1, 2));
        assertEquals("SortedSetTest.testHeadSet", headSet, set.headSet(3));
    }

    private static void testSubSet() {
        SortedSet<Integer> subSet = new TreeSet<>(Arrays.asList(1, 2));
        assertEquals("SortedSetTest.testSubSet", subSet, set.subSet(1, 3));
    }
}
