package com.getjavajob.training.algo08.korneeva.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by ZinZaga on 13.10.16.
 */
public class CollectionUtils<E> {
    public static <T> void forAllDo(Iterable<T> collection, Closure<? super T> closure) {
        if (closure != null && collection != null) {
            for (T element : collection) {
                closure.execute(element);
            }
        }
    }

    public static <T> void transform(Collection<T> collection, Transformer<? super T, ? extends T> transformer) {
        if (collection != null && transformer != null) {
            if (collection instanceof List<?>) {
                final List<T> list = (List<T>) collection;
                for (ListIterator<T> it = list.listIterator(); it.hasNext(); ) {
                    it.set(transformer.transform(it.next()));
                }
            } else {
                final Collection<T> resultCollection = collection(collection, transformer);
                collection.clear();
                collection.addAll(resultCollection);
            }
        }
    }

    public static <T, O> Collection<O> collection(Collection<T> collection,
                                                  Transformer<? super T, ? extends O> transformer) {
        if (collection != null && transformer != null) {
            Collection<O> resultCollection = new ArrayList<>(collection.size());
            for (T element : collection) {
                O newValue = transformer.transform(element);
                resultCollection.add(newValue);
            }
            return resultCollection;
        }
        return null;
    }

    public static <T> boolean filter(Iterable<T> collection, Predicate<? super T> predicate) {
        boolean result = false;
        if (collection != null && predicate != null) {
            for (Iterator<T> iterator = collection.iterator(); iterator.hasNext(); ) {
                if (!predicate.evaluate(iterator.next())) {
                    iterator.remove();
                    result = true;
                }
            }
        }
        return result;
    }

    public static <T> Collection<T> unmodifiableCollection(Collection<? extends T> collection) {
        return new UnmodifiableCollection<>(collection);
    }

    public interface Predicate<T> {

        boolean evaluate(T object);
    }

    public interface Closure<T> {

        void execute(T object);
    }

    public interface Transformer<I, O> {

        O transform(I object);
    }

    private static class UnmodifiableCollection<T> implements Collection<T> {
        final Collection<? extends T> c;

        UnmodifiableCollection(Collection<? extends T> c) {
            this.c = c;
        }

        public int size() {
            return c.size();
        }

        public boolean isEmpty() {
            return c.isEmpty();
        }

        public boolean contains(Object o) {
            return c.contains(o);
        }

        public Iterator<T> iterator() {
            return new Iterator<T>() {
                final Iterator<? extends T> it = c.iterator();

                public boolean hasNext() {
                    return it.hasNext();
                }

                public T next() {
                    return it.next();
                }

                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        public Object[] toArray() {
            return c.toArray();
        }

        public <E> E[] toArray(E[] a) {
            return c.toArray(a);
        }

        public boolean add(T e) {
            throw new UnsupportedOperationException();
        }

        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        public boolean containsAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean addAll(Collection<? extends T> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        public void clear() {
            throw new UnsupportedOperationException();
        }
    }
}
