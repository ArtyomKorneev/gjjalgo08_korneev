package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.ArrayList;
import java.util.HashMap;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 26.10.16.
 */
public class MatrixImplementationTest {
    public static void main(String[] args) {
        testMatrixFill();
    }

    private static void testMatrixFill() {
        MatrixImplementation matrix = new MatrixImplementation(new HashMap<KeyMatrix, Integer>());
        int numb = 1_000_000;
        ArrayList<Integer> compare = new ArrayList<>();
        ArrayList<Integer> res = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            matrix.set(i, numb, i);
            compare.add(i);
        }
        for (int i = 0; i < 10; i++) {
            System.out.print(matrix.get(i, numb) + " ");
        }
        assertEquals("\nMatrixTaskTest.testMatrixFill", compare, res);
    }
}
