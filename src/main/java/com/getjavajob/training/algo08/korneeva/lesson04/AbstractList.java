package com.getjavajob.training.algo08.korneeva.lesson04;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static java.lang.System.arraycopy;

/**
 * Created by ZinZaga on 06.10.16.
 */
public abstract class AbstractList<E> implements List<E> {
    private E[] elements;
    private int size;

    @Override
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return searchObject((E) o) == -1;
    }

    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return elements;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        growIfFull();
        elements[size] = e;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int i = searchObject((E) o);
        return i != -1 && rmObject(i);
    }

    boolean rmObject(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) throw new ArrayIndexOutOfBoundsException("rmObject(int i): 'i' is wrong");
        arraycopy(elements, i + 1, elements, i, size - 1 - i);
        elements[size - 1] = null;
        size--;
        return true;
    }

    public abstract boolean containsAll(Collection<?> c);

    public abstract boolean addAll(Collection<? extends E> c);

    public abstract boolean addAll(int index, Collection<? extends E> c);

    public abstract boolean removeAll(Collection<?> c);

    public abstract boolean retainAll(Collection<?> c);

    public void clear() {

    }

    @Override
    public E get(int index) throws ArrayIndexOutOfBoundsException {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): 'i' is too big");
        }
        return elements[index];
    }

    @Override
    public E set(int index, E element) throws ArrayIndexOutOfBoundsException {
        if (index >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): 'i' is too big");
        }
        E r = elements[index];
        elements[index] = element;
        return r;
    }

    @Override
    public void add(int index, E element) throws ArrayIndexOutOfBoundsException {
        if (index > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): 'i' is too big");
        }
        growIfFull();

        arraycopy(elements, index, elements, index + 1, size - index);

        elements[index] = element;
        size++;
    }

    @Override
    public E remove(int index) throws ArrayIndexOutOfBoundsException {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): 'i' is too big");
        }
        E r = elements[index];
        rmObject(index);
        return r;
    }

    boolean growIfFull() {
        if (size < elements.length - 1) {
            return false;
        }

        E[] bigger = (E[]) new Object[(int) (elements.length * 1.5)];
        arraycopy(elements, 0, bigger, 0, elements.length);
        elements = bigger;
        return true;
    }

    int searchObject(E elem) {
        for (int i = 0; i < size; i++) {
            if (elem == null && elements[i] == null) {
                return i;
            }
            if (elements[i].equals(elem)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int indexOf(Object o) throws ArrayIndexOutOfBoundsException {
        int i = searchObject((E) o);
        if (i == -1) {
            throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
        }
        return i;
    }

    public abstract int lastIndexOf(Object o);

    public abstract ListIterator<E> listIterator();

    public abstract ListIterator<E> listIterator(int index);

    public abstract List<E> subList(int fromIndex, int toIndex);
}
