package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.Arrays;
import java.util.Collections;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.11.16.
 */
public class NavigableSetTest {
    static NavigableSet<Integer> set = new TreeSet<>(Arrays.asList(1, 2, 3, 4));

    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testDescendingSet();
        testSubSet();
        testHeadSet();
        testTailSet();
    }

    private static void testLower() {
        assertEquals("NavigableSetTest.testLower", 2, set.lower(3));
    }

    private static void testFloor() {
        assertEquals("NavigableSetTest.testFloor", 3, set.floor(3));
    }

    private static void testCeiling() {
        assertEquals("NavigableSetTest.testCeiling", 2, set.ceiling(2));
    }

    private static void testHigher() {
        assertEquals("NavigableSetTest.testHigher", 3, set.higher(2));
    }

    private static void testPollFirst() {
        assertEquals("NavigableSetTest.testPollFirst", 1, set.pollFirst());
    }

    private static void testPollLast() {
        assertEquals("NavigableSetTest.testPollLast", 4, set.pollLast());
    }

    private static void testDescendingSet() {
        set.add(1);
        set.add(4);
        NavigableSet<Integer> descending = new TreeSet<>(Collections.reverseOrder());
        descending.addAll(set);
        assertEquals("NavigableSetTest.testDescendingSet", descending, set.descendingSet());
    }

    private static void testSubSet() {
        NavigableSet<Integer> subSet = new TreeSet<>(Arrays.asList(1, 2));
        assertEquals("NavigableSetTest.testSubSet", subSet, set.subSet(1, true, 2, true));
    }

    private static void testHeadSet() {
        NavigableSet<Integer> headSet = new TreeSet<>(Arrays.asList(1, 2, 3));
        assertEquals("NavigableSetTest.testHeadSet", headSet, set.headSet(3, true));
    }

    private static void testTailSet() {
        NavigableSet<Integer> tailSet = new TreeSet<>(Arrays.asList(2, 3, 4));
        assertEquals("NavigableSetTest.testTailSet", tailSet, set.tailSet(2, true));
    }
}
