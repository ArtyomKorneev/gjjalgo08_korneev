package com.getjavajob.training.algo08.korneeva.lesson09.balanced;

import com.getjavajob.training.algo08.korneeva.lesson07.binary.LinkedBinaryTree;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 13.11.16.
 */
public class RedBlackTreeTest {
    public static void main(String[] args) {
        testRedBlackTree();
    }

    private static void testRedBlackTree() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        String cmp = "0 () (1 () (2 () (3 () (4 () ()))))";
        for (int i = 0; i < 5; i++) {
            tree.add(i);
        }
        assertEquals("RedBlackTreeTest.testRedBlackTree", cmp, tree.toString((LinkedBinaryTree.NodeImpl<Integer>) tree.root()));
    }
}
