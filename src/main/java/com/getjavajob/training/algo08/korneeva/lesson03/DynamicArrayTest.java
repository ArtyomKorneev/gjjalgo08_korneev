package com.getjavajob.training.algo08.korneeva.lesson03;

import com.getjavajob.training.algo08.util.Assert;

/**
 * Created by ZinZaga on 28.09.16.
 */
public class DynamicArrayTest {
    public static void main(String[] args) {
        addTest();
        addIntTest();
        setTest();
        getTest();
        removeTest();
        removeObjTest();
        sizeTest();
        indexOfTest();
        containsTest();
        toArrayTest();
    }

    public static void addTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(45);
            arr.add(49);
            arr.add(54);
            arr.add(78);
            arr.add(56);
            Assert.fail("add(Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("add(Object e) finished correctly", e.getMessage());
        }
    }

    public static void addIntTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "b");
            arr.add(0, "a");
            arr.add(2, "arr");
            arr.add(2, "c");
            Assert.fail("add(int i, Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("add(int i, Object e) finished correctly", e.getMessage());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "b");
            arr.add(0, "a");
            arr.add(2, "arr");
            arr.add(25, "c");
            Assert.fail("add(int i, Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("add(int i, Object e): \"i\" is too big", e.getMessage());
        }
    }

    public static void setTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.set(0, "a");
            arr.set(9, "b");
            Assert.fail("set(int i, Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("set(int i, Object e): \"i\" is too big", e.getMessage());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.set(0, "a");
            arr.set(12, "b");
            Assert.fail("set(int i, Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("set(int i, Object e): \"i\" is too big", e.getMessage());
        }
    }

    public static void getTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "a");
            arr.add(1, "b");
            arr.get(0);
            arr.get(1);
            Assert.fail("get(int i) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("get(int i): \"i\" is too big", e.getMessage());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "a");
            arr.add(1, "b");
            arr.get(0);
            arr.get(3);
            Assert.fail("get(int i) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("get(int i): \"i\" is too big", e.getMessage());
        }
    }

    public static void removeTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove(0);
            arr.remove(1);
            arr.remove(2);
            Assert.fail("remove(int i) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("remove(int i): \"i\" is too big", e.getMessage());
        }
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove(0);
            arr.remove(1);
            arr.remove(10);
            Assert.fail("remove(int i) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("remove(int i): \"i\" is too big", e.getMessage());
        }
    }

    public static void removeObjTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.add("programming");
            arr.add("language");
            arr.remove("java");
            arr.remove("best");
            arr.remove("language");
            Assert.fail("remove(Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("remove(Object e) finished correctly", e.getMessage());
        }
    }

    public static void sizeTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.size();
            Assert.fail("size() finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("size() finished correctly", e.getMessage());
        }
    }

    public static void indexOfTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add("java");
            arr.add("the");
            arr.add("best");
            arr.indexOf("java");
            Assert.fail("indexOf(Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("indexOf(Object e) finished correctly", e.getMessage());
        }
    }

    public static void containsTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "java");
            arr.add(1, "the");
            arr.add(2, "best");
            arr.contains("the");
            Assert.fail("contains(Object e) finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("contains(Object e) finished correctly", e.getMessage());
        }
    }

    public static void toArrayTest() {
        try {
            DynamicArray arr = new DynamicArray();
            arr.add(0, "a");
            arr.add(1, "b");
            arr.toArray();
            Assert.fail("toArray() finished correctly");
        } catch (Exception e) {
            Assert.assertEquals("toArray() finished correctly", e.getMessage());
        }
    }
}
