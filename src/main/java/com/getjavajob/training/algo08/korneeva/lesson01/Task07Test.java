package com.getjavajob.training.algo08.korneeva.lesson01;

import static com.getjavajob.training.algo08.korneeva.lesson01.Task07.swapForBitwiseOne;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task07.swapForBitwiseTwo;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task07.swapForMathOne;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task07.swapForMathTwo;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 04.09.16.
 */
public class Task07Test {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
    }

    private static void test1() {
        assertEquals("Task07Test.test1", "12 45", swapForMathOne(45, 12));
    }

    private static void test2() {
        assertEquals("Task07Test.test2", "12 45", swapForMathTwo(45, 12));
    }

    private static void test3() {
        assertEquals("Task07Test.test3", "12 45", swapForBitwiseOne(45, 12));
    }

    private static void test4() {
        assertEquals("Task07Test.test4", "12 45", swapForBitwiseTwo(45, 12));
    }
}
