package com.getjavajob.training.algo08.korneeva.lesson05;

/**
 * Created by ZinZaga on 10.10.16.
 */
public class LinkedListQueue<E> extends AbstractQueue<E> {
    private Node<E> first;
    private Node<E> last;
    private int size;

    @Override
    public boolean add(E e) {
        Node<E> old = last;
        last = new Node<E>();
        last.val = e;
        last.next = null;
        if (first == null) {
            first = last;
        } else {
            old.next = last;
        }
        size++;
        return true;
    }

    @Override
    public E remove() {
        if (first != null) {
            E e = first.val;
            first = first.next;
            size--;
            if (first == null) {
                last = null;
            }
            return e;
        }
        return null;
    }

    public String asList() {
        String str = "";
        Node<E> node = first;
        while (node != null) {
            str += node.val + " ";
            node = node.next;
        }
        return str;
    }

    static class Node<E> {
        public Node<E> next;
        public E val;
    }
}
