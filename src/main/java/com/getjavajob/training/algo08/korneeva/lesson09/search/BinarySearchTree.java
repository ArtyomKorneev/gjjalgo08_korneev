package com.getjavajob.training.algo08.korneeva.lesson09.search;

import com.getjavajob.training.algo08.korneeva.lesson07.Node;
import com.getjavajob.training.algo08.korneeva.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (comparator != null) {
            return comparator.compare(val1, val2);
        }
        return ((Comparable<E>) val1).compareTo(val2);
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> eNode = (NodeImpl<E>) n;
        E e = eNode.getElement();
        int k = compare(e, val);
        if (k == 0) {
            return n;
        }
        if (k < 0) {
            treeSearch((Node<E>) eNode.left, val);
        } else {
            treeSearch((Node<E>) eNode.right, val);
        }

        return null;
    }

    public void changeRef(NodeImpl<E> parent, NodeImpl<E> child) {
        if (parent.parent == nil) {
            root = child;
        } else if (parent == parent.parent.left) {
            parent.parent.left = child;
        } else {
            parent.parent.right = child;
        }
        child.parent = parent.parent;
    }

    public NodeImpl<E> minNodeInTree(NodeImpl<E> node) {
        while (node.left != nil) {
            node = node.left;
        }
        return node;
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }
}
