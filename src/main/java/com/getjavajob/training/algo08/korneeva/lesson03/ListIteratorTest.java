package com.getjavajob.training.algo08.korneeva.lesson03;

/**
 * Created by ZinZaga on 29.09.16.
 */
public class ListIteratorTest {
    public static void main(String[] args) {
        Test();
    }

    public static void Test() {
        DynamicArray arr = new DynamicArray();
        DynamicArray.ListIteratorImpl listIterator = arr.listIterator();
        for (int i = 0; i < 10; i++) {
            listIterator.add(i);
        }
        while (listIterator.hasPrevious()) {
            System.out.println(listIterator.previous() + " ");
        }
        while (listIterator.hasNext()) {
            System.out.println(listIterator.next() + " ");
        }
    }
}
