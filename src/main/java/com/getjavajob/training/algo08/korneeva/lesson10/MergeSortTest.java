package com.getjavajob.training.algo08.korneeva.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.korneeva.lesson10.MergeSort.mergeSort;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class MergeSortTest {
    public static void main(String[] args) {
        testMergeSort();
    }

    private static void testMergeSort() {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
}
