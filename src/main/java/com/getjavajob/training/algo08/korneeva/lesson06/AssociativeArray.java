package com.getjavajob.training.algo08.korneeva.lesson06;

/**
 * Created by ZinZaga on 20.10.16.
 */
public class AssociativeArray<K, V> {
    static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    static final int MAXIMUM_CAPACITY = 1 << 30;
    transient int hashSeed = 0;
    private Entry[] table = new Entry[DEFAULT_INITIAL_CAPACITY];
    private int size;
    private int threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
    private float loadFactor = DEFAULT_LOAD_FACTOR;

    int hash(K k) {
        int h = hashSeed;
        if (0 != h && k instanceof String) {
            return sun.misc.Hashing.stringHash32((String) k);
        }

        h ^= k.hashCode();

        h ^= (h >>> 20) ^ (h >>> 12);
        return h ^ (h >>> 7) ^ (h >>> 4);
    }

    public int indexFor(int hash, int length) {
        return hash & (length - 1);
    }

    public V add(K key, V val) {
        V res = null;

        if (key == null) {
            return putForNullKey(val);
        }
        int hash = hash(key);
        int i = indexFor(hash, table.length);
        for (Entry<K, V> e = table[i]; e != null; e = e.next) {
            K k;
            if (e.hash == hash && ((k = e.key) == key || key.equals(k))) {
                res = e.getValue();
                e.setValue(val);
                e.value = val;
                return res;
            }
        }
        addEntry(hash, key, val, i);
        return res;
    }

    public V get(K key) {
        if (key == null) {
            return getNullKey();
        } else {
            return getEntry(key).getValue();
        }
    }

    public V remove(K key) {
        Entry<K, V> e = removeEntry(key);
        return e == null ? null : e.value;
    }

    public Entry<K, V> getEntry(K key) {
        if (size == 0) {
            return null;
        } else {
            int hash = (key == null) ? 0 : hash(key);
            for (Entry e = table[indexFor(hash, table.length)]; e != null; e = e.next) {
                if (e.hash == hash) {
                    Object k = e.key;
                    if (e.key == key || key != null && key.equals(k)) {
                        return e;
                    }
                }
            }
            return null;
        }
    }

    private V getNullKey() {
        if (this.size == 0) {
            return null;
        } else {
            for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
                if (e.key == null) {
                    return e.value;
                }
            }
            return null;
        }
    }

    private V putForNullKey(V value) {
        for (Entry<K, V> e = this.table[0]; e != null; e = e.next) {
            if (e.key == null) {
                V oldValue = e.value;
                e.value = value;
                return oldValue;
            }
        }
        addEntry(0, null, value, 0);
        return null;
    }

    void resize(int newCapacity) {

        Entry[] oldTable = table;
        int oldCapacity = oldTable.length;
        if (oldCapacity == MAXIMUM_CAPACITY) {
            threshold = Integer.MAX_VALUE;
            return;
        }

        Entry[] newTable = new Entry[newCapacity];
        transfer(newTable);
        table = newTable;
        threshold = (int) Math.min(newCapacity * loadFactor, MAXIMUM_CAPACITY + 1);
    }

    void transfer(Entry[] newTable) {
        int newCapacity = newTable.length;
        for (Entry e : table) {
            while (null != e) {
                Entry next = e.next;
                int i = indexFor(e.hash, newCapacity);
                e.next = newTable[i];
                newTable[i] = e;
                e = next;
            }
        }
    }

    void addEntry(int hash, K key, V value, int bucketIndex) {
        if (size >= threshold && null != table[bucketIndex]) {
            resize(2 * table.length);
            hash = (null != key) ? hash(key) : 0;
            bucketIndex = indexFor(hash, table.length);
        }

        createEntry(hash, key, value, bucketIndex);
    }

    void createEntry(int hash, K key, V value, int bucketIndex) {
        Entry e = table[bucketIndex];
        table[bucketIndex] = new Entry(hash, key, value, e);
        size++;
    }

    public Entry removeEntry(K key) {
        int hash = (key == null) ? 0 : hash(key);
        int i = indexFor(hash, table.length);
        Entry prev = table[i];
        Entry e;
        Entry next;

        for (e = prev; e != null; e = next) {
            next = e.next;
            if (e.hash == hash) {
                Object k = e.key;
                if (e.key == key || key != null && key.equals(k)) {
                    --size;
                    if (prev == e) {
                        table[i] = next;
                    } else {
                        prev.next = next;
                    }
                    return e;
                }
            }
            prev = e;
        }
        return e;
    }
}
