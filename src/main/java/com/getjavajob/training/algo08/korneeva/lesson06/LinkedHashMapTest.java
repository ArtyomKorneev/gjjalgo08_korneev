package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 25.10.16.
 */
public class LinkedHashMapTest {
    public static void main(String[] args) {
        Map<Integer, String> hashMap = new LinkedHashMap<>();
        hashMap.put(0, "Zero");
        assertEquals("LinkedHashMapTest.addTrue", "Zero", hashMap.put(0, "New Value"));
    }
}
