package com.getjavajob.training.algo08.korneeva.lesson06;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 25.10.16.
 */
public class SetTest {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        assertEquals("SetTest.addTestFail", false, set.add(1));
        assertEquals("SetTest.addTestPassed", true, set.add(5));
        Set<Integer> newSet = new HashSet<>();
        newSet.add(10);
        newSet.add(11);
        assertEquals("SetTest.addAllPassed", true, set.addAll(newSet));
        Set<Integer> emptySet = new HashSet<>();
        assertEquals("SetTest.AddAllFail", false, set.addAll(emptySet));
    }
}
