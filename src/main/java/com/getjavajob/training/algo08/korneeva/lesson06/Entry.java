package com.getjavajob.training.algo08.korneeva.lesson06;

/**
 * Created by ZinZaga on 24.10.16.
 */
public class Entry<K, V> {
    K key;
    V value;
    Entry<K, V> next;
    int hash;

    public Entry(int hash, K key, V value, Entry next) {
        this.hash = hash;
        this.key = key;
        this.value = value;
        this.next = next;
    }

    public final V setValue(V newValue) {
        V oldValue = this.value;
        this.value = newValue;
        return oldValue;
    }

    public K getKey() {
        return this.key;
    }

    public V getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return "Entry{" +
                "hash=" + hash +
                ", key=" + key +
                ", value='" + value + '\'' +
                ", next=" + next +
                '}';
    }
}
