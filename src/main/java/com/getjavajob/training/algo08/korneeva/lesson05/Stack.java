package com.getjavajob.training.algo08.korneeva.lesson05;

/**
 * Created by ZinZaga on 09.10.16.
 */
public interface Stack<E> {
    void push(E e); // add element to the top

    E pop(); // removes element from the top
}
