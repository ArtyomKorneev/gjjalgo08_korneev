package com.getjavajob.training.algo08.korneeva.lesson07.binary;

import com.getjavajob.training.algo08.korneeva.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    // nonpublic utility
    protected static final NodeImpl nil = new NodeImpl(null);
    public static NodeImpl root = nil;
    private int size;

    public static NodeImpl getRoot() {
        return root;
    }

    @Override
    public Collection<Node<E>> inOrder() {
        ArrayList<Node<E>> list = new ArrayList<>();
        if (root == null) {
            return null;
        }
        Stack<NodeImpl<E>> s = new Stack<>();
        NodeImpl<E> currentNode = root;
        while (!s.empty() || currentNode != null) {
            if (currentNode != null) {
                s.push(currentNode);
                currentNode = currentNode.left;
            } else {
                NodeImpl<E> n = s.pop();
                list.add(n);
                currentNode = n.right;
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> preOrder() {
        ArrayList<Node<E>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Stack<NodeImpl<E>> stack = new Stack<>();
        stack.push(root);
        while (!stack.empty()) {
            NodeImpl<E> n = stack.pop();
            list.add(n);
            if (n.right != null) {
                stack.push(n.right);
            }
            if (n.left != null) {
                stack.push(n.left);
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        ArrayList<Node<E>> list = new ArrayList<>();
        if (root == null) {
            return list;
        }
        Stack<NodeImpl<E>> stack = new Stack<>();
        stack.push(root);
        while (!stack.isEmpty()) {
            NodeImpl<E> temp = stack.peek();
            if (temp.left == null && temp.right == null) {
                NodeImpl<E> pop = stack.pop();
                list.add(pop);
            } else {
                if (temp.right != null) {
                    stack.push(temp.right);
                    temp.right = null;
                }
                if (temp.left != null) {
                    stack.push(temp.left);
                    temp.left = null;
                }
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        ArrayList<Node<E>> breadthList = new ArrayList<>();
        if (root == null) {
            System.out.println("Empty tree");
        } else {
            Queue<Node<E>> q = new LinkedList<>();
            q.add(root());
            while (q.peek() != null) {
                NodeImpl<E> temp = (NodeImpl<E>) q.remove();
                breadthList.add(temp);
                if (temp.left != null) {
                    q.add(temp.left);
                }
                if (temp.right != null) {
                    q.add(temp.right);
                }
            }
        }
        return breadthList;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        NodeImpl<E> r = new NodeImpl(e);
        r.parent = nil;
        root = r;
        return r;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> r = new NodeImpl(e);
        if (validate(n).left == null) {
            validate(n).left = new NodeImpl<E>(e);
            validate(n).left.parent = (NodeImpl<E>) n;
            size++;
        } else if (validate(n).right == null) {
            validate(n).right = new NodeImpl<E>(e);
            validate(n).right.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return r;
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> left = new NodeImpl<E>(e);
        if (validate(n).left == null) {
            validate(n).left = new NodeImpl<E>(e);
            validate(n).left.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return left;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> right = new NodeImpl<E>(e);
        if (validate(n).right == null) {
            validate(n).right = new NodeImpl<E>(e);
            validate(n).right.parent = (NodeImpl<E>) n;
            size++;
        } else {
            throw new IllegalArgumentException();
        }
        return right;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> currentNode = (NodeImpl<E>) n;
        if (currentNode == nil) {
            throw new IllegalArgumentException();
        }
        E retValue = currentNode.getElement();
        currentNode.value = e;
        return retValue;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> current = (NodeImpl<E>) n;
        if (current == nil) {
            throw new IllegalArgumentException();
        }
        if (current.parent != null && current.parent.left != null && current.parent.left == current) {
            current.parent.left = nil;
        } else if (current.parent != null && current.parent.right == current) {
            current.parent.right = nil;
        }
        size--;
        return current.getElement();
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> parent = (NodeImpl<E>) p;
        if (parent == nil) {
            throw new IllegalArgumentException();
        }
        return parent.left;
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> parent = (NodeImpl<E>) p;
        if (parent == nil) {
            throw new IllegalArgumentException();
        }
        return parent.right;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> child = (NodeImpl<E>) n;
        if (child.getElement() == null) {
            throw new IllegalArgumentException();
        }
        return child.parent;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        LinkedList<E> list = new LinkedList<>();
        for (Node<E> n : nodes()) {
            list.add(n.getElement());
        }
        return list.iterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        LinkedList<Node<E>> list = new LinkedList<>();
        LinkedList<Node<E>> extra = new LinkedList<>();
        NodeImpl<E> node = root;
        list.push(node);
        while (!list.isEmpty()) {
            node = (NodeImpl<E>) list.pop();
            extra.add(node);
            if (node.left != null) {
                list.push(node.left);
            }
            if (node.right != null) {
                list.push(node.right);
            }
        }
        return extra;
    }

    public static class NodeImpl<E> implements Node<E> {
        public boolean color = true;
        public NodeImpl<E> parent;
        public NodeImpl<E> left;
        public NodeImpl<E> right;
        public E value;

        public NodeImpl(E value) {
            this(null, null, value);
        }

        public NodeImpl(NodeImpl<E> left, NodeImpl<E> right, E value) {
            this(null, left, right, value);
        }

        public NodeImpl(NodeImpl<E> parent, NodeImpl<E> left, NodeImpl<E> right, E value) {
            this(true, parent, left, right, value);
        }

        public NodeImpl(boolean color, NodeImpl<E> parent, NodeImpl<E> left, NodeImpl<E> right, E value) {
            this.color = color;
            this.parent = parent;
            this.left = left;
            this.right = right;
            this.value = value;
        }

        @Override
        public E getElement() {
            return value;
        }
    }
}