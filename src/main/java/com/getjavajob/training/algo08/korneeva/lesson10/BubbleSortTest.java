package com.getjavajob.training.algo08.korneeva.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.korneeva.lesson10.BubbleSort.bubbleSort;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class BubbleSortTest {
    public static void main(String[] args) {
        testBubbleSort();
    }

    private static void testBubbleSort() {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
    }
}
