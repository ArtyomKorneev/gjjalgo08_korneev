package com.getjavajob.training.algo08.korneeva.lesson05;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;
import static com.getjavajob.training.algo08.util.Assert.fail;

/**
 * Created by ZinZaga on 11.10.16.
 */
public class DequeTest {
    public static void main(String[] args) {
        addFirst();
        addLast();
        offerFirst();
        offerLast();
        removeFirst();
        removeLast();
        pollFirst();
        pollLast();
        getFirst();
        getLast();
        peekFirst();
        peekLast();
        removeFirstOccurrence();
        removeLastOccurrence();
    }

    private static void addFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(1);
        deque.addFirst(0);
        assertEquals("addFirst", 0, deque.getFirst());
        String msg = "NullPointerException";
        try {
            deque.addFirst(null);
            fail(msg);
        } catch (NullPointerException e) {
            assertEquals("addFirstException", msg, e.getClass().getSimpleName());
        }
    }

    private static void addLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.addLast(1);
        assertEquals("addLast", 0, deque.getFirst());
    }

    private static void offerFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.offer(1);
        deque.offerFirst(0);
        assertEquals("offerFirst", 0, deque.getFirst());
        assertEquals("offerFirstBoolean", true, deque.offerFirst(0));
    }

    private static void offerLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.offer(0);
        deque.offerLast(1);
        assertEquals("offerLast", 1, deque.getLast());
        assertEquals("offerLastBoolean", true, deque.offerLast(1));
    }

    private static void removeFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("removeFirst", 0, deque.removeFirst());
        String msg = "NoSuchElementException";
        try {
            deque.removeFirst();
            deque.removeFirst();
            fail(msg);
        } catch (NoSuchElementException e) {
            assertEquals("removeFirstException", msg, e.getClass().getSimpleName());
        }
    }

    private static void removeLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("removeLast", 1, deque.removeLast());
    }

    private static void pollFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("pollFirst", 0, deque.pollFirst());
    }

    private static void pollLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("pollFirst", 1, deque.pollLast());
    }

    private static void getFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("getFirst", 0, deque.getFirst());
        deque.remove();
        deque.remove();
        String msg = "NoSuchElementException";
        try {
            deque.getFirst();
            fail(msg);
        } catch (NoSuchElementException e) {
            assertEquals("getFirstException", msg, e.getClass().getSimpleName());
        }
    }

    private static void getLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("getLast", 1, deque.getLast());
    }

    private static void peekFirst() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("peekFirst", 0, deque.peekFirst());
    }

    private static void peekLast() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("peekLast", 1, deque.peekLast());
    }

    private static void removeFirstOccurrence() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("removeFirstOccurrence", true, deque.removeLastOccurrence(0));
    }

    private static void removeLastOccurrence() {
        ArrayDeque<Integer> deque = new ArrayDeque();
        deque.add(0);
        deque.add(1);
        assertEquals("removeLastOccurrence", true, deque.removeLastOccurrence(1));
    }
}
