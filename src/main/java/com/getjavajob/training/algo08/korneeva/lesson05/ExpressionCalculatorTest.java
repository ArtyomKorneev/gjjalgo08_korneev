package com.getjavajob.training.algo08.korneeva.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 12.10.16.
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        testExpressionCalculator();
    }

    private static void testExpressionCalculator() {
        ExpressionCalculator calc = new ExpressionCalculator();
        assertEquals("testExpressionCalculator", 9, calc.eval("(1+(2+3*2))"));
    }
}
