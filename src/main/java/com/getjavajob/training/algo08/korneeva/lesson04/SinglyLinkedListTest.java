package com.getjavajob.training.algo08.korneeva.lesson04;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.10.16.
 */
public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testAddToSinglyList();
        testReverseSinglyList();
    }

    private static void testAddToSinglyList() {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        String result = list.asList();
        assertEquals("SinglyLinkedListTest.testAddToSinglyList", "three two one ", result);
    }

    private static void testReverseSinglyList() {
        SinglyLinkedList<String> list = new SinglyLinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.reverse();
        String result = list.asList();
        assertEquals("SinglyLinkedListTest.testReverseSinglyList", "one two three ", result);
    }
}
