package com.getjavajob.training.algo08.korneeva.lesson01;

import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.getBin;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.getBit;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.getLowerBits;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.invertBit;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.resetLowerBits;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.setBitOne;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.setBitZero;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.sumTwoNumbers;
import static com.getjavajob.training.algo08.korneeva.lesson01.Task06.toThePower;
import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 03.09.16.
 */
public class Task06Test {
    public static void main(String[] args) {
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
        test9();
    }

    private static void test1() {
        assertEquals("Task06Test.test1", 0b1000, toThePower(3));
    }

    private static void test2() {
        assertEquals("Task06Test.test2", 0b1100, sumTwoNumbers(3, 2));
    }

    private static void test3() {
        assertEquals("Task06Test.test3", 0b1000, resetLowerBits(12, 3));
    }

    private static void test4() {
        assertEquals("Task06Test.test4", 0b1101, setBitOne(9, 2));
    }

    private static void test5() {
        assertEquals("Task06Test.test5", 0b1100, invertBit(8, 2));
    }

    private static void test6() {
        assertEquals("Task06Test.test6", 0b1000, setBitZero(12, 3));
    }

    private static void test7() {
        assertEquals("Task06Test.test7", 0b11, getLowerBits(15, 2));
    }

    private static void test8() {
        assertEquals("Task06Test.test8", 0b0, getBit(9, 2));
    }

    private static void test9() {
        assertEquals("Task06Test.test9", "1100", getBin(12));
    }
}
