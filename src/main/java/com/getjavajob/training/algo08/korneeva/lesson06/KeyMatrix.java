package com.getjavajob.training.algo08.korneeva.lesson06;

/**
 * Created by ZinZaga on 26.10.16.
 */
public class KeyMatrix<V> {
    private int i;
    private int j;

    public KeyMatrix(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyMatrix<?> keyMatrix = (KeyMatrix<?>) o;

        if (i != keyMatrix.i) return false;
        return j == keyMatrix.j;

    }

    @Override
    public int hashCode() {
        int result = i;
        result = 31 * result + j;
        return result;
    }
}
