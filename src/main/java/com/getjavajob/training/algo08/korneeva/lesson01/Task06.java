package com.getjavajob.training.algo08.korneeva.lesson01;

import java.util.Scanner;

/**
 * Created by ZinZaga on 31.08.16.
 */
public class Task06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number a > 0:");
        int a = sc.nextInt();
        System.out.println("Enter the numbers 0 < n, m < 31:");
        int n = sc.nextInt();
        int m = sc.nextInt();
        System.out.println(Integer.toBinaryString(getBit(a, n)));
        System.out.println(getBin(a));
    }

    /**
     * Two to the power of n
     */
    public static int toThePower(int n) {
        return 1 << n;
    }

    /**
     * The sum of the two to the power of n and the two to the power of m
     */
    public static int sumTwoNumbers(int n, int m) {
        return 1 << n | 1 << m;
    }

    /**
     * Reset n lower bits of number a
     */
    public static int resetLowerBits(int a, int n) {
        return a & ~(1 << n - 1);
    }

    /**
     * Set a's n-th bit with 1
     */
    public static int setBitOne(int a, int n) {
        return a | 1 << n;
    }

    /**
     * Invert n-th bit of number a
     */
    public static int invertBit(int a, int n) {
        return a ^ 1 << n;
    }

    /**
     * Set a's n-th bit with 0
     */
    public static int setBitZero(int a, int n) {
        return a & ~(1 << n - 1);
    }

    /**
     * Return n lower bits
     */
    public static int getLowerBits(int a, int n) {
        return a & ~(~0 << n);
    }

    /**
     * Return n-th bit
     */
    public static int getBit(int a, int n) {
        if ((a & (1 << n - 1)) == 1 << n - 1) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Given byte a. output bin representation using bit ops
     */
    public static String getBin(int a) {
        String s = new String();
        if (a != 0) {
            s = getBin(a >> 1).concat(String.valueOf(a & 1));
        }
        return s;
    }
}
