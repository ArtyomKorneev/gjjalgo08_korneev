package com.getjavajob.training.algo08.korneeva.lesson04;

import com.getjavajob.training.algo08.util.StopWatch;

import java.util.LinkedList;

/**
 * Created by ZinZaga on 06.10.16.
 */
public class JdkListsPerformanceTest {
    public static void main(String[] args) {
        addRmvToBegin();
//        addRmvToMiddle();
//        addRmvToEnd();
    }

    private static void addRmvToBegin() {
        StopWatch timer = new StopWatch();
        timer.start();
        DoublyLinkedList<Integer> my = new DoublyLinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            my.add(0, i);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, add to begin");

        timer.start();
        LinkedList<Integer> original = new LinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            original.add(0, i);
        }
        System.out.println(timer.getElapsedTime() + " - LL, add to begin");

        timer.start();
        for (int i = 0; i < 10_000_000; i++) {
            my.remove(0);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, remove from begin");

        timer.start();
        for (int i = 0; i < 10_000_000; i++) {
            original.remove(0);
        }
        System.out.println(timer.getElapsedTime() + " - LL, remove from begin");
    }

    private static void addRmvToMiddle() {
        StopWatch timer = new StopWatch();
        timer.start();
        DoublyLinkedList<Integer> my = new DoublyLinkedList<>();
        for (int i = 0; i < 30_000; i++) {
            my.add(my.size() / 2, i);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, add to middle");

        timer.start();
        LinkedList<Integer> original = new LinkedList<>();
        for (int i = 0; i < 30_000; i++) {
            original.add(original.size() / 2, i);
        }
        System.out.println(timer.getElapsedTime() + " - LL, add to middle");

        timer.start();
        for (int i = 0; i < 30_000; i++) {
            my.remove(my.size() / 2);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, remove from middle");

        timer.start();
        for (int i = 0; i < 30_000; i++) {
            original.remove(original.size() / 2);
        }
        System.out.println(timer.getElapsedTime() + " - LL, remove from middle");
    }

    private static void addRmvToEnd() {
        StopWatch timer = new StopWatch();
        timer.start();
        DoublyLinkedList<Integer> my = new DoublyLinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            my.add(i);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, add to end");

        timer.start();
        LinkedList<Integer> original = new LinkedList<>();
        for (int i = 0; i < 10_000_000; i++) {
            original.add(i);
        }
        System.out.println(timer.getElapsedTime() + " - LL, add to end");

        timer.start();
        for (int i = 0; i < 10_000_000; i++) {
            my.remove(my.size() - 1);
        }
        System.out.println(timer.getElapsedTime() + " - DLL, remove from end");

        timer.start();
        for (int i = 0; i < 10_000_000; i++) {
            original.remove(original.size() - 1);
        }
        System.out.println(timer.getElapsedTime() + " - LL, remove from end");
    }
}
