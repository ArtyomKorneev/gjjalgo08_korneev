package com.getjavajob.training.algo08.korneeva.lesson06;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 24.10.16.
 */
public class AssociativeArrayTest {
    public static void main(String[] args) {
        testAddToArray();
        testAddNullToArray();
        testRemove();
    }

    private static void testRemove() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "One");
        array.add(2, "Two");
        array.add(3, "Three");
        assertEquals("AssociativeArrayTest.testRemove", "Three", array.remove(3));
    }

    private static void testAddToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "One");
        array.add(2, "Two");
        array.add(3, "Three");
        assertEquals("AssociativeArrayTest.testAddToArray", "Two", array.get(2));
    }

    private static void testAddNullToArray() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(null, "One");
        assertEquals("AssociativeArrayTest.testAddNullToArray", "One", array.get(null));
    }
}
