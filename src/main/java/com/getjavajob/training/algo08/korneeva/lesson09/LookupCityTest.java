package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.SortedSet;

/**
 * Created by ZinZaga on 13.03.17.
 */
public class LookupCityTest {
    public static void main(String[] args) {
        searchForTest();
    }

    private static void searchForTest() {
        SortedSet<String> result = LookupCity.searchFor().subSet("mo", true, "mo" + Character.MAX_VALUE, true);
        for (String s : result) {
            System.out.println(s);
        }
    }
}
