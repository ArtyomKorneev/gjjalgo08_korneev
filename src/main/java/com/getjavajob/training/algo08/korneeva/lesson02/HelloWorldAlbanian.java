package com.getjavajob.training.algo08.korneeva.lesson02;

import java.io.UnsupportedEncodingException;

/**
 * @author Vital Severyn
 */
public class HelloWorldAlbanian {
    public static void main(String[] args) throws UnsupportedEncodingException {
        System.out.println("P�rsh�ndetje bot�!");

        String original = "P�rsh�ndetje bot�!!";
        String s = new String(original.getBytes(), "Cp1250");
        System.out.println(s);
    }
}
