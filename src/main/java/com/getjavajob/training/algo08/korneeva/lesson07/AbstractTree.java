package com.getjavajob.training.algo08.korneeva.lesson07;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Stack;

/**
 * An abstract base class providing some functionality of the Tree interface
 *
 * @param <E> element
 */
public abstract class AbstractTree<E> implements Tree<E> {
    @Override
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) > 0;
    }

    @Override
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        return childrenNumber(n) == 0;
    }

    @Override
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        return root() != null;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new ElementIterator();
    }

    /**
     * @return an iterable collection of nodes of the tree in preorder
     */
    public Collection<Node<E>> preOrder() {
        if (size() == 0) {
            return new ArrayList<>();
        } else {
            return preOrder(root(), new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> preOrder(Node<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            collection.add(node);
            Iterator<Node<E>> itr = children(node).iterator();
            preOrder(itr.next(), collection);
            preOrder(itr.next(), collection);
        }
        return collection;
    }

    /**
     * @return an iterable collection of nodes of the tree in postorder
     */
    public Collection<Node<E>> postOrder() {
        if (size() == 0) {
            return new ArrayList<>();
        } else {
            return postOrder(root(), new ArrayList<Node<E>>());
        }
    }

    private Collection<Node<E>> postOrder(Node<E> node, Collection<Node<E>> collection) {
        if (node == null) {
            return collection;
        } else {
            Iterator<Node<E>> itr = children(node).iterator();
            postOrder(itr.next(), collection);
            postOrder(itr.next(), collection);
            collection.add(node);
        }
        return collection;
    }

    /**
     * @return an iterable collection of nodes of the tree in breadth-first order
     */
    public Collection<Node<E>> breadthFirst() {
        Collection<Node<E>> collection = new ArrayList<>();
        if (root() == null) {
            return collection;
        }
        Stack<Node<E>> stack = new Stack<>();
        stack.push(root());
        while (!stack.empty()) {
            Node<E> n = stack.pop();
            collection.add(n);
            for (Node<E> node : children(n)) {
                if (node != null) {
                    stack.push(node);
                }
            }
        }
        return collection;
    }

    /**
     * Adapts the iteration produced by {@link Tree#nodes()}
     */
    private class ElementIterator implements Iterator<E> {
        private Iterator<Node<E>> it = nodes().iterator();

        @Override
        public boolean hasNext() {
            return it.hasNext();
        }

        @Override
        public E next() {
            return it.next().getElement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
