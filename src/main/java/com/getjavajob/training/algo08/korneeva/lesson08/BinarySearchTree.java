package com.getjavajob.training.algo08.korneeva.lesson08;

import com.getjavajob.training.algo08.korneeva.lesson07.Node;
import com.getjavajob.training.algo08.korneeva.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (val1 != null && val2 != null) {
            if (comparator != null) {
                return comparator.compare(val1, val2);
            }
            return ((Comparable<E>) val1).compareTo(val2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> element = (NodeImpl<E>) n;
        if (element == null) {
            return null;
        }
        int result = compare(element.getElement(), val);
        if (result == 0) {
            return element;
        } else if (result > 0) {
            return treeSearch(element.left, val);
        } else if (result < 0) {
            return treeSearch(element.right, val);
        } else {
            return null;
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }

}
