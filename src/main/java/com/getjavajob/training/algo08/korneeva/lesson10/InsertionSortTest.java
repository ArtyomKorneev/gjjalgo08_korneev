package com.getjavajob.training.algo08.korneeva.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.algo08.korneeva.lesson10.InsertionSort.sort;

/**
 * Created by ZinZaga on 16.11.16.
 */
public class InsertionSortTest {
    public static void main(String[] args) {
        testInsertionSort();
    }

    private static void testInsertionSort() {
        int[] array = {3, 5, 6, 8, 0, 1, 2, 4, 7, 9};
        sort(array);
        System.out.println(Arrays.toString(array));
    }
}
