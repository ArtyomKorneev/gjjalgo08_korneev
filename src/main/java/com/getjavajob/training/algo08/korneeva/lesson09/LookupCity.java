package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * Created by ZinZaga on 13.11.16.
 */
public class LookupCity {
    public static NavigableSet<String> searchFor() {
        NavigableSet<String> storageOfCities = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
        storageOfCities.add("Moscow");
        storageOfCities.add("Mogilev");
        storageOfCities.add("Volgograd");
        storageOfCities.add("Sochi");
        storageOfCities.add("Rostov-on-Don");
        return storageOfCities;
    }
}
