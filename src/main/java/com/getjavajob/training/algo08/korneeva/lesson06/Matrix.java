package com.getjavajob.training.algo08.korneeva.lesson06;

/**
 * Created by ZinZaga on 25.10.16.
 */
public interface Matrix<V> {

    V get(int i, int j);

    void set(int i, int j, V value);
}
