package com.getjavajob.training.algo08.korneeva.lesson04;

/**
 * Created by ZinZaga on 06.10.16.
 */
public class SinglyLinkedList<E> {
    public Node<E> head;
    private int size;

    public void add(E e) {
        Node<E> node = new Node<>();
        node.val = e;
        if (head == null) {
            head = node;
        } else {
            node.next = head;
            head = node;
        }
        size++;
    }

    public E get(int index) {
        checkIndex(index);
        Node<E> node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.val;
    }

    public void checkIndex(int index) {
        if (index < 0 || index >= size)
            throw new IndexOutOfBoundsException(outOfBoundMsg(index));
    }

    public String outOfBoundMsg(int index) {
        return "Index = " + index + ", Size = " + size;
    }

    public int size() {
        return size;
    }

    public void reverse() {
        if (head == null)
            return;
        Node<E> node = head.next;
        while (node != null) {
            Node<E> next = node.next;
            relink(node);
            node = next;
        }
    }

    public void relink(Node<E> relinkedNode) {
        Node<E> prev = null;
        Node<E> curr = head;
        while (curr != relinkedNode) {
            prev = curr;
            curr = curr.next;
        }
        prev.next = curr.next;
        curr.next = head;
        head = curr;
    }

    public String asList() {
        StringBuilder s = new StringBuilder();
        Node<E> node = head;
        while (node != null) {
            s.append(node.val + " ");
            node = node.next;
        }
        return String.valueOf(s);
    }

    public static class Node<E> {
        public Node<E> next;
        public E val;
    }
}