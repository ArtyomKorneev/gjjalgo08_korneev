package com.getjavajob.training.algo08.korneeva.lesson04;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by ZinZaga on 03.10.16.
 */
public class DoublyLinkedList<E> implements java.util.List<E> {
    private Element<E> head = new Element<>();
    private Element<E> tail = new Element<>();
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        if (isEmpty()) {
            return false;
        } else if (head.data.equals(o)) {
            return true;
        }
        Element<E> t = head;
        while (t.next != null) {
            t = t.next;
            if (t.data.equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Iterator iterator()");
    }

    @Override
    public Object[] toArray() {
        Object[] mas = new Object[size];
        mas[0] = head.data;
        Element<E> t = head;
        int i = 1;
        while (t.next != null) {
            t = t.next;
            mas[i++] = t.data;
        }
        return mas;
    }

    @Override
    public boolean add(E o) {
        Element<E> a = new Element<>();
        a.data = o;
        if (isEmpty()) {
            head = a;
            tail = a;
        } else {
            a.prev = tail;
            tail.next = a;
            tail = a;
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (isEmpty()) {
            return false;
        } else if (head == tail && head.data.equals(o)) {
            tail = null;
            head = null;
            size--;
            return true;
        } else if (head.data.equals(o)) {
            head = head.next;
            head.prev = null;
            size--;
            return true;
        } else if (tail.data.equals(o)) {
            tail.prev.next = null;
            tail = tail.prev;
            size--;
            return true;
        } else {
            Element<E> t;
            t = head;
            while (t.next != null && !t.data.equals(o)) {
                t = t.next;
                if (t.data.equals(o)) {
                    t.next.prev = t.prev;
                    t.prev.next = t.next;
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean addAll(Collection collection)");
    }

    @Override
    public boolean addAll(int i, Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean addAll(int i, Collection collection)");
    }

    @Override
    public void clear() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("void clear()");
    }

    @Override
    public E get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): i is too big");
        } else if (i <= size / 2) {
            Element<E> t = head;
            for (int j = 0; j < i; j++) {
                t = t.next;
            }
            return t.data;
        } else {
            Element<E> t = tail;
            for (int j = size - 1; j > i; j--) {
                t = t.prev;
            }
            return t.data;
        }
    }

    @Override
    public E set(int i, E o) throws ArrayIndexOutOfBoundsException {
        Element<E> t = new Element<>();
        get(i);
        t.data = o;
        return t.data;
    }

    @Override
    public void add(int i, E o) {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("void add(int i, Object o): i is too big");
        }
        Element<E> a = new Element<>();
        a.data = o;
        if (i == 0) {
            a.next = head;
            head = a;
        } else if (i > 0 && i < size) {
            Element<E> t = head;
            for (int j = 0; j < i - 1; j++) {
                t = t.next;
            }
            a.next = t.next;
            a.prev = t;
            t.next = a;
        } else if (i == size) {
            tail.next = a;
            a.prev = tail;
            tail = a;
        }
        size++;
    }

    @Override
    public E remove(int i) {
        Element<E> res;
        if (i == 0) {
            res = head;
            head = head.next;
            size--;
            return (E) res;
        } else if (i > size / 2) {
            Element<E> t = head;
            for (int k = 0; k < i - 1; k++) {
                t = t.next;
            }
            res = t.next;
            t.next = t.next.next;
            if (t.next == null) {
                tail = t;
            }
            size--;
        } else {
            Element<E> t = tail;
            for (int k = size - 1; k > i - 1; k--) {
                t = t.prev;
            }
            res = t.prev;
            t.prev = t.prev.prev;
            t.next = t.next.next;
            if (t.prev == null) {
                head = t;
            }
            size--;
        }
        return (E) res;
    }

    @Override
    public int indexOf(Object o) throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("list is empty");
        }
        if (head.data.equals(o)) {
            return 0;
        }
        int count = 0;
        Element<E> t = head;
        while (!t.data.equals(o) && t.next != null) {
            t = t.next;
            count++;
            if (t.data.equals(o)) {
                return count;
            }
        }
        if (count == size - 1) {
            throw new NoSuchElementException("Element<E> has not being found");
        }
        return count;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("int lastIndexOf(Object o)");
    }

    @Override
    public ListIterator<E> listIterator() {
        return new ListIteratorImpl<>();
    }

    @Override
    public ListIterator<E> listIterator(int i) {
        return new ListIteratorImpl<>();
    }

    @Override
    public List<E> subList(int i, int i1) {
        throw new UnsupportedOperationException("List subList(int i, int i1)");
    }

    @Override
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("boolean retainAll(Collection collection)");
    }

    @Override
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("boolean removeAll(Collection collection)");
    }

    @Override
    public boolean containsAll(Collection collection) {
        throw new UnsupportedOperationException("boolean containsAll(Collection collection)");
    }

    @Override
    public Object[] toArray(Object[] objects) {
        throw new UnsupportedOperationException("Object[] toArray(Object[] objects)");
    }

    public static class ListIteratorImpl<E> implements ListIterator<E> {

        int listIndex;
        private DoublyLinkedList<E> d = new DoublyLinkedList<>();

        @Override
        public boolean hasNext() {
            return listIndex < d.size() - 1;
        }

        @Override
        public E next() {
            return d.get(++listIndex);
        }

        @Override
        public boolean hasPrevious() {
            return listIndex - 1 >= 0;
        }

        @Override
        public E previous() {
            return d.get(--listIndex);
        }

        @Override
        public int nextIndex() {
            return ++listIndex;
        }

        @Override
        public int previousIndex() {
            return --listIndex;
        }

        @Override
        public void remove() {
            d.remove(listIndex);
        }

        @Override
        public void set(E o) {
            d.set(listIndex, o);
        }

        @Override
        public void add(E o) {
            d.add(o);
        }
    }

    private static class Element<T> {
        Element<T> next;
        Element<T> prev;
        T data;
    }
}
