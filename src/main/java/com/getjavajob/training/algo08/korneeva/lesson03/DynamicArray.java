package com.getjavajob.training.algo08.korneeva.lesson03;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * Created by ZinZaga on 27.09.16.
 */
public class DynamicArray {
    private Object[] elements;
    private int size;
    private int modCount;

    public DynamicArray() {
        this(10);
    }

    public DynamicArray(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("DynamicArray(int i): i < 0");
        } else {
            elements = new Object[i];
            size = 0;
        }
    }

    public Object elementArray(int i) {
        return elements[i];
    }

    boolean add(Object e) {
        grow();
        elements[size++] = e;
        return true;
    }

    void add(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): \"i\" is too big");
        }
        grow();
        System.arraycopy(elements, i, elements, i + 1, size - i);
        elements[i] = e;
        size++;
    }

    boolean grow() {
        if (size + 1 < elements.length) {
            return false;
        } else {
            Object[] big = new Object[elements.length * 3 / 2 + 1];
            System.arraycopy(elements, 0, big, 0, elements.length);
            elements = big;
            return true;
        }
    }

    Object set(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): \"i\" is too big");
        }
        Object result = elements[i];
        elements[i] = e;
        return result;
    }

    Object get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): \"i\" is too big");
        }
        return elements[i];
    }

    Object remove(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): \"i\" is too big");
        }
        Object result = elements[i];
        rmv(i);
        return result;
    }

    boolean remove(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    rmv(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    rmv(i);
                    return true;
                }
            }
        }
        return false;
    }

    boolean rmv(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) {
            throw new ArrayIndexOutOfBoundsException("rmv(int i): \"i\" is wrong");
        }
        System.arraycopy(elements, i + 1, elements, i, size - 1 - i);
        elements[--size] = null;
        return true;
    }

    int size() {
        return size;
    }

    int indexOf(Object e) throws ArrayIndexOutOfBoundsException {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    return i;
                }
            }
        }
        throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
    }

    boolean contains(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    return true;
                }
            }
        }
        return false;
    }

    Object[] toArray() {
        return elements;
    }

    public DynamicArray.ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public class ListIteratorImpl implements ListIterator {
        int cursor;
        private int prevIndex = -1;
        private int expectedModCount = modCount;

        ListIteratorImpl(int index) {
            super();
            cursor = index;
        }

        public boolean hasNext() {
            return cursor != size;
        }

        public Object next() {
            checkForConcurrentModification();
            int i = cursor;
            if (i >= size) {
                throw new NoSuchElementException();
            }
            Object[] mas = DynamicArray.this.elements;
            if (i >= mas.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i + 1;
            return elementArray(i);
        }

        public boolean hasPrevious() {
            return cursor > 0;
        }

        public Object previous() {
            checkForConcurrentModification();
            int i = cursor - 1;
            if (i < 0) {
                throw new NoSuchElementException();
            }
            Object[] elements = DynamicArray.this.elements;
            if (i >= elements.length) {
                throw new ConcurrentModificationException();
            }
            cursor = i;
            return elementArray(prevIndex = i);
        }

        public int nextIndex() {
            return cursor;
        }

        public int previousIndex() {
            return cursor - 1;
        }

        public void remove() {
            if (prevIndex < 0) {
                throw new IllegalStateException();
            }
            checkForConcurrentModification();

            try {
                DynamicArray.this.remove(prevIndex);
                cursor = prevIndex;
                prevIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void set(Object e) {
            if (prevIndex < 0) {
                throw new IllegalStateException();
            }
            checkForConcurrentModification();
            try {
                DynamicArray.this.set(prevIndex, e);
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        public void add(Object e) {
            checkForConcurrentModification();
            try {
                int i = cursor;
                DynamicArray.this.add(i, e);
                cursor = i + 1;
                prevIndex = -1;
                expectedModCount = modCount;
            } catch (IndexOutOfBoundsException ex) {
                throw new ConcurrentModificationException();
            }
        }

        final void checkForConcurrentModification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
