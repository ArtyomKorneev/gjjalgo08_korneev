package com.getjavajob.training.algo08.korneeva.lesson05;

import com.getjavajob.training.algo08.korneeva.lesson04.SinglyLinkedList;

import java.util.EmptyStackException;

/**
 * Created by ZinZaga on 07.10.16.
 */
public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> list;
    private int top = -1;

    public LinkedListStack() {
        this.list = new SinglyLinkedList<>();
    }

    @Override
    public void push(E val) {
        list.add(val);
        top++;
    }

    @Override
    public E pop() throws EmptyStackException {
        if (top < 0) {
            throw new EmptyStackException();
        } else {
            SinglyLinkedList.Node<E> res = list.head;
            list.head = list.head.next;
            top--;
            return res.val;
        }
    }

    public String print() {
        return list.asList();
    }
}
