package com.getjavajob.training.algo08.korneeva.lesson09;

import java.util.SortedMap;
import java.util.TreeMap;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 11.11.16.
 */
public class SortedMapTest {
    private static SortedMap<Integer, String> map = new TreeMap<>();

    public static void main(String[] args) {
        mapInit();
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
    }

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
    }

    private static void testSubMap() {
        SortedMap<Integer, String> subMap = new TreeMap<>();
        subMap.put(1, "one");
        subMap.put(2, "two");
        assertEquals("SortedMapTest.testSubMap", subMap, map.subMap(1, 3));
    }

    private static void testHeadMap() {
        SortedMap<Integer, String> headMap = new TreeMap<>();
        headMap.put(1, "one");
        headMap.put(2, "two");
        assertEquals("SortedMapTest.testHeadMap", headMap, map.headMap(3));
    }

    private static void testTailMap() {
        SortedMap<Integer, String> tailMap = new TreeMap<>();
        tailMap.put(4, "four");
        tailMap.put(5, "five");
        assertEquals("SortedMapTest.testTailMap", tailMap, map.tailMap(4));
    }

    private static void testFirstKey() {
        assertEquals("SortedMapTest.testFirstKey", 1, map.firstKey());
    }

    private static void testLastKey() {
        assertEquals("SortedMapTest.testLastKey", 5, map.lastKey());
    }
}
