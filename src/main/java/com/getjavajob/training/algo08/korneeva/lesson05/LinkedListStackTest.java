package com.getjavajob.training.algo08.korneeva.lesson05;

import static com.getjavajob.training.algo08.util.Assert.assertEquals;

/**
 * Created by ZinZaga on 10.10.16.
 */
public class LinkedListStackTest {
    public static void main(String[] args) {
        testPush();
        testPop();
    }

    private static void testPush() {
        LinkedListStack<String> stack = new LinkedListStack<>();
        stack.push("one");
        stack.push("two");
        stack.push("three");
        assertEquals("LinkedListStackTest.testPush", "three two one ", stack.print());
    }

    private static void testPop() {
        LinkedListStack<String> stack = new LinkedListStack<>();
        stack.push("one");
        stack.push("two");
        stack.push("three");
        stack.pop();
        assertEquals("LinkedListStackTest.testPop", "two one ", stack.print());
    }
}
