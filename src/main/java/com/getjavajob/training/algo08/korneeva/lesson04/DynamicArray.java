package com.getjavajob.training.algo08.korneeva.lesson04;

/**
 * Created by ZinZaga on 03.10.16.
 */
public class DynamicArray<E> {
    private Object[] elements;
    private int size;

    public DynamicArray() {
        this(10);
    }

    public DynamicArray(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("DynamicArray(int i): i < 0");
        } else {
            elements = new Object[i];
            size = 0;
        }
    }

    boolean add(Object e) {
        grow();
        elements[size++] = e;
        return true;
    }

    void add(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): \"i\" is too big");
        }
        grow();
        System.arraycopy(elements, i, elements, i + 1, size - i);
        elements[i] = e;
        size++;
    }

    boolean grow() {
        if (size + 1 < elements.length) {
            return false;
        } else {
            Object[] big = new Object[elements.length * 3 / 2 + 1];
            System.arraycopy(elements, 0, big, 0, elements.length);
            elements = big;
            return true;
        }
    }

    Object set(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i >= elements.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): \"i\" is too big");
        }
        Object result = elements[i];
        elements[i] = e;
        return result;
    }

    Object get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): \"i\" is too big");
        }
        return elements[i];
    }

    Object remove(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): \"i\" is too big");
        }
        Object result = elements[i];
        rmv(i);
        return result;
    }

    boolean remove(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    rmv(i);
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    rmv(i);
                    return true;
                }
            }
        }
        return false;
    }

    boolean rmv(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) {
            throw new ArrayIndexOutOfBoundsException("rmv(int i): \"i\" is wrong");
        }
        System.arraycopy(elements, i + 1, elements, i, size - 1 - i);
        elements[--size] = null;
        return true;
    }

    int size() {
        return size;
    }

    int indexOf(Object e) throws ArrayIndexOutOfBoundsException {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    return i;
                }
            }
        }
        throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
    }

    boolean contains(Object e) {
        if (e == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return true;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(e)) {
                    return true;
                }
            }
        }
        return false;
    }

    Object[] toArray() {
        return elements;
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(1);
    }

    public class ListIteratorImpl implements java.util.ListIterator {
        int cursor = 0;
        DynamicArray<E> arr;

        public ListIteratorImpl(int i) {
            arr = new DynamicArray<>(i);
        }

        @Override
        public boolean hasNext() {
            return (cursor < arr.size());
        }

        @Override
        public Object next() {
            return arr.elements[cursor++];
        }

        @Override
        public boolean hasPrevious() {
            return (cursor > 0);
        }

        @Override
        public Object previous() {
            cursor--;
            return arr.elements[cursor];
        }

        @Override
        public int nextIndex() {
            return cursor++;
        }

        @Override
        public int previousIndex() {
            return cursor--;
        }

        @Override
        public void remove() {
            arr.rmv(cursor);
        }

        @Override
        public void set(Object e) {
            arr.set(cursor, e);
        }

        @Override
        public void add(Object e) {
            arr.add(cursor, e);
            cursor++;
        }
    }
}
