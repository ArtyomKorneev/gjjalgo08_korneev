package com.getjavajob.training.algo08.korneeva.lesson04;

import com.getjavajob.training.algo08.util.Assert;

import java.util.NoSuchElementException;

/**
 * Created by ZinZaga on 04.10.16.
 */
public class DoublyLinkedListTest {
    public static void main(String[] args) {
        addtoArray();
        isEmpty();
        contains();
        get();
        set();
        addIo();
        indexOf();
        iterAddHasNext();
        iterHasPrevious();
        iterNextPreviousIndex();
        iterRemove();
        iterSet();
    }

    public static void addtoArray() {
        System.out.println("addtoArray");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        d.add("one");
        d.add("two");
        d.add("three");
        String str = "";
        System.out.println(d.size());
        for (Object a : d.toArray()) {
            str += a;
            str += " ";
        }
        Assert.assertEquals(str, "one two three ");
    }

    public static void isEmpty() {
        System.out.println("isEmpty");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        Assert.assertEquals(d.isEmpty(), true);
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.isEmpty(), false);
    }

    public static void contains() {
        System.out.println("contains");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        Assert.assertEquals(d.contains("two"), false);
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.contains("one"), true);
        Assert.assertEquals(d.contains("two"), true);
        Assert.assertEquals(d.contains("three"), true);
    }

    public static void get() {
        System.out.println("get");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.get(0), "one");
        Assert.assertEquals(d.get(1), "two");
        Assert.assertEquals(d.get(2), "three");
        try {
            Assert.assertEquals(d.get(3), "four");
        } catch (ArrayIndexOutOfBoundsException e) {
            Assert.assertEquals(e.getMessage(), "get(int i): i is too big");
        }
    }

    public static void set() {
        System.out.println("set");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        d.add("");
        d.add("");
        d.add("");
        Assert.assertEquals(d.set(0, "one"), "one");
        Assert.assertEquals(d.set(1, "two"), "two");
        Assert.assertEquals(d.set(2, "three"), "three");
        try {
            Assert.assertEquals(d.set(3, "four"), "four");
        } catch (ArrayIndexOutOfBoundsException e) {
            Assert.assertEquals(e.getMessage(), "get(int i): i is too big");
        }
    }

    public static void addIo() {
        System.out.println("addIo");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        d.add(0, "zero");
        d.add(1, "one");
        d.add(2, "three");
        d.add(2, "two");
        try {
            d.add(5, "three");
        } catch (ArrayIndexOutOfBoundsException e) {
            Assert.assertEquals(e.getMessage(), "void add(int i, Object o): i is too big");
        }
        String str = "";
        System.out.println("list size = " + d.size());
        for (Object a : d.toArray()) {
            str += a;
            str += " ";
        }
        Assert.assertEquals(str, "zero one two three ");
    }

    public static void indexOf() {
        System.out.println("indexOf");
        DoublyLinkedList<String> d = new DoublyLinkedList<>();
        try {
            d.indexOf("zero");
        } catch (NoSuchElementException e) {
            Assert.assertEquals(e.getMessage(), "list is empty");
        }
        d.add("zero");
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.indexOf("zero"), 0);
        Assert.assertEquals(d.indexOf("one"), 1);
        Assert.assertEquals(d.indexOf("two"), 2);
        Assert.assertEquals(d.indexOf("three"), 3);
        try {
            Assert.assertEquals(d.indexOf("four"), -1);
        } catch (NoSuchElementException e) {
            Assert.assertEquals(e.getMessage(), "element has not being found");
        }
    }

    public static void iterAddHasNext() {
        System.out.println("iterHasNext");
        DoublyLinkedList.ListIteratorImpl<String> d = new DoublyLinkedList.ListIteratorImpl<>();
        d.add("one");
        d.add("two");
        d.add("three");

        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "two");
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "three");
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "four");
        }
    }

    public static void iterHasPrevious() {
        System.out.println("iterHasPrevious");
        DoublyLinkedList.ListIteratorImpl<String> d = new DoublyLinkedList.ListIteratorImpl<>();
        d.add("one");
        d.add("two");
        d.add("three");
        d.next();
        d.next();
        if (d.hasPrevious()) {
            Assert.assertEquals(d.previous(), "two");
        }
        if (d.hasPrevious()) {
            Assert.assertEquals(d.previous(), "one");
        }
        if (d.hasPrevious()) {
            Assert.assertEquals(d.previous(), "zero");
        }
    }

    public static void iterNextPreviousIndex() {
        System.out.println("iterNextPreviousIndex");
        DoublyLinkedList.ListIteratorImpl<String> d = new DoublyLinkedList.ListIteratorImpl<>();
        d.add("one");
        d.add("two");
        d.add("three");
        if (d.hasNext()) {
            Assert.assertEquals(d.nextIndex(), 1);
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.nextIndex(), 2);
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.nextIndex(), 3);
        }
    }

    public static void iterRemove() {
        System.out.println("iterRemove");
        DoublyLinkedList.ListIteratorImpl<String> d = new DoublyLinkedList.ListIteratorImpl<>();
        d.add("one");
        d.add("two");
        d.add("three");
        d.previousIndex();
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "one");
        }
        d.remove();
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "three");
        }
    }

    public static void iterSet() {
        System.out.println("iterSet");
        DoublyLinkedList.ListIteratorImpl<String> d = new DoublyLinkedList.ListIteratorImpl<>();
        d.add("one");
        d.add("two");
        d.add("three");
        if (d.hasNext()) {
            d.next();
        }
        d.set("twoSetted");
        d.previous();
        d.previous();
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "one");
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "twoSetted");
        }
        if (d.hasNext()) {
            Assert.assertEquals(d.next(), "three");
        }
    }

}
