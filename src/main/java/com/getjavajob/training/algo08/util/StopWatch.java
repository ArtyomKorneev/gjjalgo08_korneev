package com.getjavajob.training.algo08.util;

/**
 * Created by ZinZaga on 29.09.16.
 */
public class StopWatch {
    public long before;
    public long after;

    public long start() {
        before = System.currentTimeMillis();
        return before;
    }

    public long getElapsedTime() {
        after = System.currentTimeMillis();
        return after - before;
    }
}
