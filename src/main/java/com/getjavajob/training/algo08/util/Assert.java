package com.getjavajob.training.algo08.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by ZinZaga on 01.09.16.
 */
public class Assert {
    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + "failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

////////////////////////////////////

    public static void assertEquals(String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println("passed");
        } else {
            System.out.println("failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println("passed");
        } else {
            System.out.println("failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(int expected, int actual) {
        if (expected == actual) {
            System.out.println("passed");
        } else {
            System.out.println("failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, List<String> expected, List<String> actual) {
        boolean flag = true;

        if (expected != null && actual != null && expected.size() == actual.size() && expected.size() > 0) {
            for (int i = 0; i < actual.size(); i++) {
                if (!expected.get(i).equals(actual.get(i))) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }
        if (flag) {
            System.out.println(testName + " passed");
        } else {
            String ex = new String();
            String ac = new String();
            for (String s : expected) {
                ex = ex + s;
            }
            for (String s : actual) {
                ac = ac + s;
            }
            System.out.println(testName + " failed: expected " + ex + ", actual " + ac);
        }
    }

    public static void assertEquals(String testName, Integer[] expected, Integer[] actual) {
        boolean b = true;

        if (expected.length == actual.length) {
            for (int i = 0; i < expected.length; i++) {
                if (expected[i] != actual[i]) {
                    b = false;
                }
            }
        }

        if (b) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected ");
            for (int k : expected) {
                System.out.print(k + " ");
            }

            System.out.println("\n actual ");
            for (int k : actual) {
                System.out.print(k + " ");
            }
        }
    }

    public static void assertEquals(String testName, ArrayList<Integer> expected, ArrayList<Integer> actual) {
        boolean flag = true;
        if (expected != null && actual != null && expected.size() == actual.size() && expected.size() > 0) {
            for (int i = 0; i < actual.size(); i++) {
                if (expected.get(i) != actual.get(i)) {
                    flag = false;
                }
            }
        } else {
            flag = false;
        }

        if (flag) {
            System.out.println("\n" + testName + " passed");
        } else {
            String ex = new String();
            String ac = new String();
            for (Integer s : expected) {
                ex = ex + s + " ";
            }
            for (Integer s : actual) {
                ac = ac + s + " ";
            }
            System.out.println(testName + " failed: expected " + ex + ", actual " + ac);
        }
    }

    public static void assertEquals(String testName, Collection<?> expected, Collection<?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, Map<?, ?> expected, Map<?, ?> actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void fail(String msg) {
        throw new AssertionError(msg);
    }
}
